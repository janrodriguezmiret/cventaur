# CVentaur

![Alt CVentaur logo](frontend/src/assets/img/120146-200.png? "CVentaur")
![Alt EPSEVG logo](frontend/src/assets/img/logo-epsevg.png? "EPSEVG")

CVentaur is a prototype platform where people with no expertise in artificial intelligence can collaborate to create machine learning models oriented to object detection in images, with the purpose of accelerating, improving and avoiding errors in this kind of tasks. It is aimed to take advantage of the knowledge of these people and to digitise it so that it can be used and shared more easily, while they are also benefitting.


CVentaur is the result of the Bachelor's Thesis of Jan Rodríguez Miret, an Informatics Engineer student from Escola Superior Politècnica de Vilanova i la Geltrú (EPSEVG).

**NOTE:** This is a prototype and so it has a very poor performance, as it is not optimized and does not scale well. You may also experience difficulties while installing it.

## Prerequisites

This project was built and tested over Ubuntu 18.04 x64. If it is not your OS, take this into consideration and try to ensure that it will work in your OS. 

You will need a **sudo** user.

To begin with, make sure you have the following general packages installed:
 - node
 - npm
 - python3
 - python3-venv

## Installation


Start by cloning this repository.

```bash
git clone https://bitbucket.org/janrodriguezmiret/cventaur.git
```

The complete system has 3 major modules (Frontend, Main Server and DB Server) plus 2 more services: Auth0 and MongoDB database. Each of them has its own installation and configuration process, explained below. 

By default, all services are in localhost and the following ports are used:
- 3000 Frontend
- 9090 Main Server
- 8080 DB Server

If you want to use another address or port, you will have to configure it manually.

### Authentication

The authentication of the system is taken by Auth0. You can create a free acount and configure an API in their website: https://manage.auth0.com/

You will have to allow the addresses that you use for your Frontend and Main Server.


### Frontend

It's a website built in React that allows the users to interact with the systems.

1.To install React and all the dependencies needed for the frontend

```bash
cd frontend
npm install
```

2.Modify the Auth0 configuration file ```frontend/src/auth_config.json``` to match the API you created above.

3.(optional) If your Main Server is not located in localhost:9090, you'll have to change the variable baseURL from ```frontend/src/api/index.js``` to match it.


### Main Server

It's the entry point of the backend of the system and contains the filesystem of CVentaur (images, anotations, models, etc.). It also has a sub-service called Worker, that is in charge for executing the predictions and exports in the background.

1.Create a virtual environment

```bash
cd main_server
python3 -m venv .
```

2.Activate virtual environment

```bash
source bin/activate
```

3.Install dependencies

```bash
pip install -r requirements.txt
```

This installation process may take a while.

### MongoDB

1.Install MongoDB, as the official guide say (https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/#install-mongodb-community-edition):

```bash
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
```
```bash
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu \ bionic/mongodb-org/4.2 multiverse" \ 
| sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
```
```bash
sudo apt-get update
```
```bash
sudo apt-get install -y mongodb-org
```
```bash
sudo systemctl start mongod
```
```bash
sudo systemctl enable mongod
```

2.Configure it to use a replica

Modify the replication specified in the configuration file ```/etc/mongod.conf``` with the following:
```
replication:
  replSetName: "rs0"
```

Restart the service
```bash
sudo systemctl restart mongod
```

Initiate the replica using mongo shell
```bash
mongo
```
```mongodb
rs.initiate()
```

3.Ensure that mongodb user is the owner of the socket and the rest of files needed

```bash
sudo chown -R mongodb:mongodb /var/lib/mongodb
```

```bash
sudo chown mongodb:mongodb /tmp/mongodb-27017.sock
```

## Usage

To use the whole system, you will need to initiate all services manually, but it's pretty straightforward. You will need **one** terminal for each service (Frontend, Main Server, Worker and DB Server)

### Frontend

Open a new terminal and go to the specified directory
```bash
cd frontend
```

Initiate the React development server
```bash
npm start
```

### Main Server

Open a new terminal and go to the specified directory
```bash
cd main_server
```

Initiate the Flask development server
```bash
python server.py
```


### Worker

Open a new terminal and go to the specified directory
```bash
cd main_server
```

Initiate the Worker service
```bash
python worker.py
```


### DB Server

Open a new terminal and go to the specified directory
```bash
cd db_server
```

Initiate the Express development server
```bash
npm start
```

## License

This project its under a Creative Commons BY-NC-SA License. Find more details in https://creativecommons.org/licenses/by-nc-sa/2.0/.

![Alt BY-NC-SA License](frontend/src/assets/img/creative_commons.png? "License")

