import React, { useEffect } from "react"

const defaultGlobalState = {
  activeCollection: null
};

const globalStateContext = React.createContext(defaultGlobalState);
const dispatchStateContext = React.createContext(undefined);

export const useGlobalState = () => [
  React.useContext(globalStateContext),
  React.useContext(dispatchStateContext)
];
const localState = JSON.parse(localStorage.getItem("localState"))
export const GlobalStateProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(
    (state, newValue) => ({ ...state, ...newValue }),
    localState || defaultGlobalState
  );
  useEffect(() => {
    localStorage.setItem("localState",JSON.stringify(state));
  },[state])
  return (
    <globalStateContext.Provider value={state}>
      <dispatchStateContext.Provider value={dispatch}>
        {children}
      </dispatchStateContext.Provider>
    </globalStateContext.Provider>
  );
};