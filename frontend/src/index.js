/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
// added by Jan
import { Auth0Provider } from "./react-auth0-spa";
import config from "./auth_config.json";
import PrivateRoute from "components/PrivateRoute/PrivateRoute";
import * as serviceWorker from "./serviceWorker";

// core components
import Admin from "layouts/Admin.js";
import Home from "layouts/Home.js";

import "assets/css/material-dashboard-react.css?v=1.8.0";
import { GlobalStateProvider } from "globalstate";

const hist = createBrowserHistory();

// A function that routes the user to the right place
// after login
const onRedirectCallback = appState => {
  hist.push(
    appState && appState.targetUrl
      ? appState.targetUrl
      : window.location.pathname
  );
};

ReactDOM.render(
  <Auth0Provider
    domain={config.domain}
    client_id={config.clientId}
    redirect_uri={window.location.origin}
    audience={config.audience}
    onRedirectCallback={onRedirectCallback}
  >
    <GlobalStateProvider>
      <Router history={hist}>
        <Switch>
          <Route path="/" exact component={Home} />
          <PrivateRoute path="/admin" component={Admin} />
        </Switch>
      </Router>
    </GlobalStateProvider>
  </Auth0Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
