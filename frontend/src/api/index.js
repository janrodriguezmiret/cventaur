import Axios from 'axios'

const baseURL = 'http://localhost:9090/api'
const apiMain = Axios.create({
    baseURL: baseURL,
})

const getConfig = (token) => {
    return {
        headers: {
            Authorization: `Bearer ${token}`
        }
    }
}

export const createUser = (payload) => apiMain.post(`/user`, payload)
export const getAllUsers = (token) => apiMain.get(`/users`, getConfig(token))
export const getUserById = (token, id) => apiMain.get(`/user/${id}`, getConfig(token))
export const createCollection = (token, collection) => apiMain.post(`/collections`, { collection }, getConfig(token))
export const getCollectionsByOwner = (token, owner) => apiMain.get(`/collections/owner/${owner}`, getConfig(token))
export const deleteCollection = (token, collection_id) => apiMain.delete(`/collections/${collection_id}`, getConfig(token))
export const getBaseCollections = (token) => apiMain.get(`/collections/base`, getConfig(token))
export const getAllTags = (token) => apiMain.get(`/tags`, getConfig(token))
export const getJobs = (token, collection_id, confirmed) => apiMain.get(`/collections/${collection_id}/jobs?confirmed=${confirmed}`, getConfig(token))
export const createJob = (token, collection_id, formData) => apiMain.post(`/collections/${collection_id}/jobs`, formData, {
    headers: {
        'content-type': 'multipart/form-data',
        Authorization: `Bearer ${token}`
    }
})
export const getValidations = (token, jobId) => apiMain.get(`/validations/job/${jobId}`, getConfig(token))
export const getJobImageURL = (collectionId, jobId, filename) => `${baseURL}/collections/${collectionId}/jobs/${jobId}/images/${filename}`
export const updateJobValidations = (token, jobId, validations) => apiMain.patch(`/validations/job/${jobId}`, { validations }, getConfig(token))
export const updateJob = (token, jobId, jobChanges) => apiMain.patch(`/jobs/${jobId}`, { jobChanges }, getConfig(token))
export const addResources = (collection_id, formData) => apiMain.post(`/collections/${collection_id}/resources`, formData, {
    headers: {
        'content-type': 'multipart/form-data'
    }
})
export const createExport = (token, jobIds, collectionId) => apiMain.post(`/exports`, { jobIds, collectionId }, getConfig(token))
export const getExportsByCollection = (token, collectionId) => apiMain.get(`/exports/collection/${collectionId}`, getConfig(token))
export const getExportURL = (filename, collectionId) => `${baseURL}/exports/collection/${collectionId}/download/${filename}`
export const createModel = (token, collection_id, formData) => apiMain.post(`/models/collection/${collection_id}`, formData, {
    headers: {
        'content-type': 'multipart/form-data',
        Authorization: `Bearer ${token}`
    }
})
export const getUploadModelGuideURL = `${baseURL}/templates/cventaur.ipynb`
