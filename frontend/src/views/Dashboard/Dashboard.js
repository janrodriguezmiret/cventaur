import React from "react";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import FitnessCenter from "@material-ui/icons/FitnessCenter";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import Visibility from "@material-ui/icons/Visibility";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";


import {
  aiPerformance,
  classesPerformance,
  imagesPredicted,
} from "../../variables/charts";

import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import Snackbar from "components/Snackbar/Snackbar";

const useStyles = makeStyles(styles);

export default function Dashboard() {
  const classes = useStyles();
  return (
    <div>
      <Snackbar
        open={true}
        place="tc"
        message="This page is for demo purposes only. The data displayed is not real"
        color="danger" />
      <GridContainer>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="warning" stats icon>
              <CardIcon color="warning">
                <Icon>content_copy</Icon>
              </CardIcon>
              <p className={classes.cardCategory}>Used Space</p>
              <h3 className={classes.cardTitle}>
                49/50 <small>GB</small>
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <Danger>
                  <Warning />
                </Danger>
                <a href="#space" onClick={e => e.preventDefault()}>
                  Get more space
                </a>
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="success" stats icon>
              <CardIcon color="success">
                <FitnessCenter />
              </CardIcon>
              <p className={classes.cardCategory}>GPU Usage</p>
              <h3 className={classes.cardTitle}>23 hours</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <DateRange />
                This month
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="danger" stats icon>
              <CardIcon color="danger">
                <Visibility />
              </CardIcon>
              <p className={classes.cardCategory}>CPU Usage</p>
              <h3 className={classes.cardTitle}>75 hours</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <Danger>
                  <Warning />
                </Danger>
                <a href="#resources" onClick={e => e.preventDefault()}>
                  Get more resources
                </a>
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="info" stats icon>
              <CardIcon color="info">
                <Accessibility />
              </CardIcon>
              <p className={classes.cardCategory}>Collaborators</p>
              <h3 className={classes.cardTitle}>12</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <Update />
                3 online
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
      <GridContainer>
        <GridItem xs={12} sm={12} md={4}>
          <Card chart>
            <CardHeader color="success">
              <ChartistGraph
                className="ct-chart"
                data={aiPerformance.data}
                type="Line"
                options={aiPerformance.options}
                listener={aiPerformance.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>AI performance</h4>
              <p className={classes.cardCategory}>
                <span className={classes.successText}>
                  <ArrowUpward className={classes.upArrowCardCategory} /> 12%
                </span>{" "}
                increase since last training.
              </p>
            </CardBody>
            <CardFooter chart>
              <div className={classes.stats}>
                <AccessTime /> updated 4 minutes ago
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={4}>
          <Card chart>
            <CardHeader color="warning">
              <ChartistGraph
                className="ct-chart"
                data={classesPerformance.data}
                type="Bar"
                options={classesPerformance.options}
                responsiveOptions={classesPerformance.responsiveOptions}
                listener={classesPerformance.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Accuracy by class</h4>
              <p className={classes.cardCategory}>Last prediction launched</p>
            </CardBody>
            <CardFooter chart>
              <div className={classes.stats}>
                <AccessTime /> prediction sent 2 days ago
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={4}>
          <Card chart>
            <CardHeader color="danger">
              <ChartistGraph
                className="ct-chart"
                data={imagesPredicted.data}
                type="Line"
                options={imagesPredicted.options}
                listener={imagesPredicted.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Images predicted</h4>
              <p className={classes.cardCategory}>Last week</p>
            </CardBody>
            <CardFooter chart>
              <div className={classes.stats}>
                <AccessTime /> updated 10 seconds ago
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Collaborators Stats</h4>
              <p className={classes.cardCategoryWhite}>
                Summary of the last month
              </p>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="warning"
                tableHead={["User", "Images predicted", "IoU from AI", "Last connection"]}
                tableData={[
                  ["Dakota Rice", "230", "82,54%", "An hour ago"],
                  ["Minerva Hooper", "78", "80,21%", "Yesterday, 4:34pm"],
                  ["Sage Rodriguez", "10", "14,60%", "Tuesday 13 March"],
                  ["Philip Chaney", "125", "76,43%", "Friday 12 April"]
                ]}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
