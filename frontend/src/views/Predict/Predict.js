import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { useAuth0 } from "../../react-auth0-spa"
import { useAsyncEffect } from "use-async-effect"
import { useGlobalState } from "../../globalstate"
import Button from "components/CustomButtons/Button.js";
import { Table, TableRow, TableCell, TableHead, TableBody } from "@material-ui/core";
import Refresh from "@material-ui/icons/Refresh"
import { useDropzone } from "react-dropzone"
import { getJobs, createJob } from "../../api"
import Card from "components/Card/Card";
import CardHeader from "components/Card/CardHeader";
import CardBody from "components/Card/CardBody";
import { NavLink } from "react-router-dom";
import Moment from "react-moment"
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Snackbar from "components/Snackbar/Snackbar";



const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  dropzone: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "20px",
    borderWidth: "2px",
    borderRadius: "2px",
    borderColor: "#bbbbbb",
    borderStyle: "dashed",
    backgroundColor: "#fafafa",
    color: "#999999",
    outline: "none",
    transition: "border .24s ease-in-out",
  },
  hint: {
    color: "#9d9d9d",
    fontSize: "14px",
  }
};

const useStyles = makeStyles(styles);

export default function Predict() {
  const classes = useStyles();
  const { getTokenSilently, loading } = useAuth0()
  const [state] = useGlobalState()
  const [jobs, setJobs] = useState([])
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({ accept: 'image/png ,image/jpeg' })

  // Snackbar to inform successes or errors in operations
  const closeSnackbar = () => setOptionsSnackbar({ ...optionsSnackbar, open: false })
  const [optionsSnackbar, setOptionsSnackbar] = useState({ open: false, place: "bc", close: true, closeNotification: closeSnackbar })
  const showSnackbar = (success, message) => setOptionsSnackbar({ ...optionsSnackbar, open: true, color: success ? "success" : "danger", message: message })

  useAsyncEffect(async isMounted => refresh(isMounted), [loading, getTokenSilently]);

  const refresh = async (isMounted) => {
    const token = await getTokenSilently()
    if (!loading) {
      try {
        const response = await getJobs(token, state.activeCollection._id, false)
        if (!isMounted) return;
        setJobs(response.data.data.jobs)
      }
      catch (error) {
        console.log(error)
        showSnackbar(false, error.message)
      }
    }
  }

  const uploadFiles = async () => {
    if (acceptedFiles.length > 50){
      return showSnackbar(false, "The maximum number of images in a prediction is 50. Please try again with less.")
    }
    const formData = new FormData()
    acceptedFiles.forEach((image) => formData.append('images', image))
    try {
      const token = await getTokenSilently()
      const response = await createJob(token, state.activeCollection._id, formData)
      showSnackbar(response.status === 201, response.data.message)
      refresh(true)
    }
    catch (error) {
      console.log(error)
      showSnackbar(false, error.message)
    }
  }

  var displayJobs = () => {
    if (!jobs.length) {
      return (
          <p className={classes.hint}>There aren't any job created. You can create one by uploading your images and clicking 'Predict'</p>
      )
    }
    return jobs.map((job, key) => {
      const percentagePredicted = (job.numPredicted * 100 / (job.numTotal))
      const date = new Date(job.creationDate)
      return (
        <TableRow key={key}>
          <TableCell>
            {job.state === "processed" ?
              (<NavLink to={`/admin/predict/results/${job._id}`}>
                {job._id}
              </NavLink>)
              : job._id}
          </TableCell>
          <TableCell>
            <Moment format="DD/MM/YYYY - HH:mm">{date}</Moment>
          </TableCell>
          <TableCell>
            {job.state}
          </TableCell>
          <TableCell>
            {job.numPredicted.toString() + "/" + job.numTotal.toString() + ` (${percentagePredicted}%)`}
          </TableCell>
        </TableRow>
      );
    })
  }

  return (
    <div>
    <GridContainer>
      <GridItem xs={12} sm={12} md={6}>
        <div {...getRootProps({ className: classes.dropzone })}>
          <input {...getInputProps()} />
          <p>Drag and drop some images here, or click to select files</p>
        </div>
      </GridItem>
      <GridItem xs={12} sm={12} md={6}>
        <Button color="primary" disabled={acceptedFiles.length === 0} onClick={uploadFiles}>Predict</Button>
        {acceptedFiles.length !== 0 && <p>{acceptedFiles.length} file/s selected</p>}
      </GridItem>
      <Card>
        <CardHeader color="primary">
          <h4 className={classes.cardTitleWhite}>Predictions</h4>
        </CardHeader>
        <CardBody>
          <Table className={classes.table}>
            <TableHead>
              <Button color="primary" onClick={() => refresh(true)}><Refresh />Refresh</Button>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell>Created</TableCell>
                <TableCell>State</TableCell>
                <TableCell>Processed images</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {displayJobs()}
            </TableBody>
          </Table>
        </CardBody>
      </Card>
    </GridContainer>
    <Snackbar {...optionsSnackbar} />
    </div>
  );
}
