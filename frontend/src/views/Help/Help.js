import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import CardHeader from "components/Card/CardHeader";
import Card from "components/Card/Card";
import CardBody from "components/Card/CardBody";
import { NavLink } from "react-router-dom";
import { List, ListItem } from "@material-ui/core";
import { getUploadModelGuideURL } from "../../api/index"


const styles = {
  step: {
    marginBottom: "40px",
  },
  detailStep: {
    marginLeft: "5%"
  },
  note: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    bottom: "10px",
    color: "#c0c1c2",
    display: "block",
    fontWeight: "400",
    fontSize: "13px",
    lineHeight: "13px",
    left: "0",
    marginLeft: "20px",
    position: "absolute",
    width: "260px"
  },
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
};

const useStyles = makeStyles(styles);

export default function Help() {
  const classes = useStyles();
  const items = [
    {
      title: "I cannot see any object detected from the AI",
      id: "i-cannot-see-object",
      body: (
        <div>
          <div className={classes.step}>
            <p>You won't get any prediction until you have trained your model once.</p>
            <p>Otherwise, the model doesn't know what to look for.</p>
          </div>
        </div>
      ),
    },
    {
      title: "How can I train the model?",
      id: "how-can-i-train-the-model",
      body: (
        <div>
          <div className={classes.step}>
            <p>The following steps require a minimum expertise in programming and AI. They are not intended for a common user.</p>
            <p>This is an adaptation from <a href="https://medium.com/deepquestai/train-object-detection-ai-with-6-lines-of-code-6d087063f6ff">this guide</a>.
               Please refer to FAQs or contact an expert if you have any doubts.</p>
          </div>
          <div className={classes.step}>
            <p>1. Export the predictions from which you want the model to be trained.</p>
            <p className={classes.detailStep}>Go to <NavLink target="_blank" to="/admin/dataset"> Dataset</NavLink> page, select the predictions from the Summary tab and click on the Export button.</p>
            <p className={classes.detailStep}>It is recommended to use at least 200 images and its annotations.</p>
          </div>
          <div className={classes.step}>
            <p>2. Once the export has finished, download the corresponding .zip file from the Exports tab.</p>
          </div>
          <div className={classes.step}>
            <p>3. Unzip the file.</p>
          </div>
          <div className={classes.step}>
            <p>4. Create a directory with the name you want (e.g. 'vehicles'). This will be your root directory.</p>
          </div>
          <div className={classes.step}>
            <p>5. Inside your root directory create two subdirectories named 'train' and 'validation', each one with two subdirectories named 'images' and 'annotations'.</p>
          </div>
          <div className={classes.step}>
            <p>6. Divide your exported dataset manually into 'train' and 'validation'.</p>
            <p className={classes.detailStep}>It is recommended to put roughly 80%-20% of your images and their corresponding annotations into 'train' and 'validation' respectively.</p>
          </div>
          <div className={classes.step}>
            <p>7. Upload your root directory to the root level of you Google Drive filesystem.</p>
            <p className={classes.detailStep}>It won't work if it's not placed inside any other folder.</p>
          </div>
          <div className={classes.step}>
            <p>8. Download the notebook used to train <a href={getUploadModelGuideURL}>here</a>.</p>
          </div>
          <div className={classes.step}>
            <p>9. Go to <a target="_blank" rel="noopener noreferrer" href="https://colab.research.google.com">https://colab.research.google.com</a> and open/import the notebook you've just downloaded.</p>
            <p className={classes.detailStep}>Make sure to change the runtime to GPU for a better performance.</p>
          </div>
          <div className={classes.step}>
            <p>10. Follow the steps in the notebook and execute the cells accordingly.</p>
            <p className={classes.detailStep}>Download both the generated model (.h5) and the configuration file (.json) as specified.</p>
          </div>
          <div className={classes.step}>
            <p>11. Upload your new model.</p>
            <p className={classes.detailStep}>Go to <NavLink target="_blank" to="/admin/aibuilder"> AI Builder</NavLink> page and upload the model and the configuration files.</p>
          </div>
        </div>
      ),
    },
    {
      title: "Why can't I see Predict, Dataset, etc. pages on the menu?",
      id: "why-cant-i-see-menu",
      body: (
        <div>
          <div className={classes.step}>
            <p>You probably don't have any active collection. Make sure to activate one in My Collections or Public Collections.</p>
          </div>
        </div>
      ),
    },
    {
      title: "I cannot see my prediction on the list, it has disappeared",
      id: "i-cannot-see-prediction",
      body: (
        <div>
          <div className={classes.step}>
            <p>Your predictions are shown either in Predict or in Dataset pages, depending on whether they have been confirmed or not. Check both pages.</p>
          </div>
        </div>
      ),
    },
    {
      title: "The tag that I need is not shown",
      id: "tag-that-i-need",
      body: (
        <div>
          <div className={classes.step}>
            <p>For now, the tags displayed are just a name, so you can choose another tag.</p>
            <p>We are working on increasing the kind of tags available to select.</p>
          </div>
        </div>
      ),
    },

  ]


  return (
    <div>
      <h4>FAQs</h4>
      <List>
        {items.map((item,index) => {
          return (
            <ListItem>
              <a href={`#${item.id}`}>{index+1}. {item.title}</a>
            </ListItem>
          )
        })}
      </List>

      {items.map(item => {
        return (
          <Card id={item.id}>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>{item.title}</h4>
            </CardHeader>
            <CardBody>
              {item.body}
            </CardBody>
          </Card>
        )
      })}

    </div>
  );
}
