import React, { useState, useEffect } from "react";
import { useAuth0 } from "../../react-auth0-spa"
import { useGlobalState } from "../../globalstate"
//import Button from "components/CustomButtons/Button.js";
import ReactImageAnnotate from "react-image-annotate";
import Button from "../../components/CustomButtons/Button";
import { getValidations, getJobImageURL, updateJobValidations, updateJob } from "../../api/index"
import { useAsyncEffect } from "use-async-effect"
import DoneAll from "@material-ui/icons/DoneAll"
import { DialogActions, Dialog, DialogTitle, DialogContent } from "@material-ui/core";
import Snackbar from "components/Snackbar/Snackbar";
import GridItem from "components/Grid/GridItem";
import GridContainer from "components/Grid/GridContainer";

const styles = {
  summary: {
    marginTop: "20px",
    marginBottom: "20px"
  },
}

export default function Result(props) {
  const { getTokenSilently, loading } = useAuth0()
  const [validations, setValidations] = useState([])
  const [isOpen, setIsOpen] = useState(false)
  const [tagIds, setTagIds] = useState({})
  const [state] = useGlobalState()
  const [job, setJob] = useState()
  const [summary, setSummary] = useState({})
  const jobId = props.match.params.jobId

  // Snackbar to inform successes or errors in operations
  const closeSnackbar = () => setOptionsSnackbar({ ...optionsSnackbar, open: false })
  const [optionsSnackbar, setOptionsSnackbar] = useState({ open: false, place: "bc", close: true, closeNotification: closeSnackbar })
  const showSnackbar = (success, message) => setOptionsSnackbar({ ...optionsSnackbar, open: true, color: success ? "success" : "danger", message: message })

  // Save a dictionary to translate className to its id faster
  useEffect(() => {
    let newTagIds = {}
    state.activeCollection.tags.forEach(tag => {
      newTagIds[tag.className] = tag._id
    });
    setTagIds(newTagIds)
  }, [state])

  // Update summary if validations are updated and tagIds are already existing
  useEffect(() => {
    if (Object.keys(tagIds).length === 0 || !validations.length) {
      return
    }
    let newSummary = { total: 0 }
    // Initialise to 0 all classes
    Object.keys(tagIds).forEach(className => {
      newSummary[className] = 0
    })
    validations.forEach(validation => {
      validation.regions.forEach(object => {
        let classNames = object.cls.split(" ")
        // Remove the percentage (if existing) from the className to get the name
        if (object.cls.indexOf("%") > 0) {
          classNames.pop()
        }
        const className = classNames.join(" ")
        newSummary[className] = newSummary[className] + 1
        newSummary["total"] = newSummary["total"] + 1
      })
    })
    // In case nothing was found in the prediction show notification
    if (newSummary["total"]=== 0){
      const message = `There aren't any objects detected in this prediction.
      If you have enough validated data, make sure to train the model to get predictions from the AI.
      Please refer to Help for more information`
      showSnackbar(false,message)
    }
    setSummary(newSummary)
    // eslint-disable-next-line
  }, [validations, tagIds])

  useAsyncEffect(async isMounted => getJobValidations(isMounted), [loading, getTokenSilently]);
  const getJobValidations = async (isMounted) => {
    const token = await getTokenSilently()
    if (!loading) {
      try {
        const response = await getValidations(token, jobId)
        const jobResponse = response.data.data.job
        const validationsResponse = response.data.data.validations
        if (!isMounted) return;
        const images = validationsResponse.map(validation => {
          let image = {}
          image.src = getJobImageURL(state.activeCollection._id, jobId, validation.image.filename)
          image.regions = validation.objects.map(object => {
            let region = {
              type: "box",
              highlighted: false,
              editingLabels: false,
              color: "hsl(92,100%,50%)",
              prob: object.prob
            }
            region.id = object._id
            region.cls = object.tag.className
            if (object.prob !== 100) {
              region.cls = region.cls + " " + object.prob.toFixed(2) + "%"
            }
            region.x = object.bbox[0] * 1.0 / validation.image.width
            region.y = object.bbox[1] * 1.0 / validation.image.height
            region.w = (object.bbox[2] - object.bbox[0]) * 1.0 / validation.image.width
            region.h = (object.bbox[3] - object.bbox[1]) * 1.0 / validation.image.height
            return region
          })
          image.pixelSize = {
            w: validation.image.width,
            h: validation.image.height
          }
          image._id = validation._id
          return image
        })
        setValidations(images)
        setJob(jobResponse)
      }
      catch (error) {
        console.log(error)
        showSnackbar(false, error.message)
      }
    }
  }

  const toggleModal = () => {
    setIsOpen(!isOpen)
  }

  const saveValidations = async (MainLayoutState) => {
    const newValidations = {}
    try {
      MainLayoutState.images.forEach((newValidation, index) => {
        const validation = validations[index]
        const objects = newValidation.regions.map(region => {
          const existentRegion = validation.regions.find(reg => reg.id === region.id)
          let prob = 100
          let classNames = region.cls.split(" ")
          // If contains percentage it's a prediction, remove the percentage from the className and update it
          if (region.cls.indexOf("%") > 0) {
            classNames.pop()
            prob = existentRegion.prob
          }
          const className = classNames.join(" ")
          // Retrieve prob
          let newObject = {
            tag: tagIds[className],
            prob: prob,
            bbox: []
          }
          newObject.bbox.push(Math.round(newValidation.pixelSize.w * region.x))
          newObject.bbox.push(Math.round(newValidation.pixelSize.h * region.y))
          newObject.bbox.push(Math.round((newValidation.pixelSize.w * region.w) + newObject.bbox[0]))
          newObject.bbox.push(Math.round((newValidation.pixelSize.h * region.h) + newObject.bbox[1]))
          return newObject
        })
        newValidations[validations[index]._id] = objects
      })
      const token = await getTokenSilently()
      const response = await updateJobValidations(token, jobId, newValidations)
      setValidations(MainLayoutState.images)
      showSnackbar(response.status === 200, response.data.message)
    }
    catch (error) {
      console.log(error)
      showSnackbar(false, error.message)
    }
  }


  const confirmValidations = async () => {
    try {
      const token = await getTokenSilently()
      let response = await updateJob(token, jobId, { state: "confirmed" })
      showSnackbar(response.status === 200, response.data.message)
      if (response.status === 200) {
        setJob({ ...job, state: "confirmed" })
        toggleModal()
      }
    }
    catch (error) {
      console.log(error)
      showSnackbar(false, error.message)
    }
  }

  var displayAnnotator = () => {
    if (validations.length) {
      return (
        <div>
          <ReactImageAnnotate
            selectedImage={validations[0].src}
            images={validations}
            regionClsList={state.activeCollection.tags.map(tag => tag.className)}
            onExit={saveValidations}
            enabledTools={["select", "create-box"]}
            selectedTool={"create-box"}
          />
        </div>
      )
    }
    return null
  }

  var displaySummary = () => {
    if (Object.keys(summary).length) {
      return (
        <div style={styles.summary}>
          <GridContainer styles={styles.summary}>
            {Object.keys(summary).map(className => {
              return (
                <GridItem xs={6} sm={6} md={2}>
                  <b>{className}</b>: {summary[className].toString()}
                </GridItem>
              )
            })}
          </GridContainer>
        </div>
      )
    }
  }

  return (
    <div>
      <Button color="primary" onClick={toggleModal} disabled={!job || job.state !== "processed"}><DoneAll />Confirm</Button>
      {displaySummary()}
      <Dialog open={isOpen} onClose={toggleModal} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Confirm all validations</DialogTitle>
        <DialogContent>
          <p>Before you confirm, make sure to save the changes you've made.</p>
          <p>Once confirmed, the prediction will appear in the Dataset page.</p>
        </DialogContent>
        <DialogActions>
          <Button onClick={toggleModal} color="danger">
            Cancel
          </Button>
          <Button onClick={confirmValidations} color="success">
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
      {displayAnnotator()}
      <Snackbar {...optionsSnackbar} />
    </div>
  );
}

