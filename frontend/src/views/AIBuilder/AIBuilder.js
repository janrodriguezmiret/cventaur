import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { useAuth0 } from "../../react-auth0-spa"
import { useGlobalState } from "../../globalstate"
import CardHeader from "components/Card/CardHeader";
import Card from "components/Card/Card";
import CardBody from "components/Card/CardBody";
import Button from "components/CustomButtons/Button";
import { NavLink } from "react-router-dom";
import Publish from "@material-ui/icons/Publish"
import GridItem from "components/Grid/GridItem";
import GridContainer from "components/Grid/GridContainer";
import CardFooter from "components/Card/CardFooter";
import { createModel } from "../../api/index";
import ReactLoading from 'react-loading';
import Snackbar from "../../components/Snackbar/Snackbar";


const styles = {
  field: {
    fontWeight: "bold",
    marginTop: "40px"
  },
  note: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    bottom: "10px",
    color: "#c0c1c2",
    display: "block",
    fontWeight: "400",
    fontSize: "13px",
    lineHeight: "13px",
    left: "0",
    marginLeft: "20px",
    position: "absolute",
    width: "260px"
  },
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
};

const useStyles = makeStyles(styles);

export default function AIBuilder() {
  const classes = useStyles();
  const { getTokenSilently } = useAuth0()
  const [state] = useGlobalState()
  const [configFile, setConfigFile] = useState()
  const [modelFile, setModelFile] = useState()
  const [isLoading, setIsLoading] = useState(false)

  // Snackbar to inform successes or errors in operations
  const closeSnackbar = () => setOptionsSnackbar({ ...optionsSnackbar, open: false })
  const [optionsSnackbar, setOptionsSnackbar] = useState({ open: false, place: "bc", close: true, closeNotification: closeSnackbar })
  const showSnackbar = (success, message) => setOptionsSnackbar({ ...optionsSnackbar, open: true, color: success ? "success" : "danger", message: message })

  const uploadFiles = async () => {
    setIsLoading(true)
    const token = await getTokenSilently()
    const formData = new FormData()
    formData.append('model', modelFile)
    formData.append('config', configFile)
    try {
      const response = await createModel(token, state.activeCollection._id, formData)
      showSnackbar(response.status === 201, response.data.message)
    }
    catch (error) {
      console.log(error)
      showSnackbar(false, error.message)
    }
    setIsLoading(false)
  }

  const onChangeModelFile = async (event) => {
    if (event.target.files) {
      setModelFile(event.target.files[0])
    }
  }

  const onChangeConfigFile = async (event) => {
    if (event.target.files) {
      setConfigFile(event.target.files[0])
    }
  }

  const loader = (
    <div>
      <ReactLoading color="grey" type="spin" />
      <p>Uploading... This process can take some minutes, please wait.</p>
    </div>
  )

  return (
    <div>
      <Card>
        <CardHeader color="primary">
          <h4 className={classes.cardTitleWhite}>Upload a new model</h4>
        </CardHeader>
        <CardBody>
          <p>First, make sure to train the model by following the steps detailed
            <NavLink target="_blank" to="help"> here</NavLink>.
          </p>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <p className={classes.field}>Model</p>
              <p>e.g. detection_model-ex-034--loss-0004.104.h5</p>
              <input onChange={onChangeModelFile} type="file" />
            </GridItem>
            <GridItem xs={12} sm={12} md={12}>
              <p className={classes.field}>Detection config</p>
              <p>e.g. detection_config.json</p>
              <input onChange={onChangeConfigFile} type="file" accept="application/json" />
            </GridItem>
            <GridItem xs={12} sm={12} md={12}>
              <p></p>
              {isLoading && loader}
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button disabled={!modelFile || !configFile} color="primary" onClick={uploadFiles}><Publish />Upload</Button>
        </CardFooter>
      </Card>
      <Snackbar {...optionsSnackbar} />
    </div>
  );
}
