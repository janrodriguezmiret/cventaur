import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Button from "components/CustomButtons/Button.js";
import { Table, TableHead, TableRow, TableCell, TableBody } from "@material-ui/core";
import CustomInput from "components/CustomInput/CustomInput";
import { useGlobalState } from "../../globalstate"
import Search from "@material-ui/icons/Search"



const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
};

const useStyles = makeStyles(styles);

export default function PublicCollections() {
  const classes = useStyles();
  const [state, dispatch] = useGlobalState()
  const [collections] = useState([])

  var displayCollections = () => {
    if (!collections.length) {
      console.log("NO COLLECTIONS")
      return <p>There are no public collections at the moment.</p>
    }
    return collections.map((collection, key) => {
      return (
        <TableRow
          key={key}
          style={{
            backgroundColor: state.activeCollection && state.activeCollection._id === collection._id ? "lightgrey" : "white",
          }}
          onClick={() => dispatch({ activeCollection: collection })}>
          <TableCell>
            {collection.name}
          </TableCell>
          <TableCell>
            {collection.tags.map(tag => tag.className).join(", ")}
          </TableCell>
          <TableCell>
            {collection.owner}
          </TableCell>
        </TableRow>
      );
    })
  }

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Public collections</h4>
            </CardHeader>
            <CardBody>
              <div>
                <CustomInput
                  formControlProps={{
                    className: classes.margin + " " + classes.search
                  }}
                  inputProps={{
                    placeholder: "Search by name",
                    inputProps: {
                      "aria-label": "Search"
                    },
                    disabled: true
                  }}
                />
                <Button color="white" aria-label="edit" justIcon round disabled>
                  <Search />
                </Button>
              </div>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Tags</TableCell>
                    <TableCell>Owner</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {displayCollections()}
                </TableBody>
              </Table>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
