import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import CardHeader from "components/Card/CardHeader";
import Card from "components/Card/Card";
import CardBody from "components/Card/CardBody";


const styles = {
  step: {
    marginBottom: "40px",
  },
  detailStep: {
    marginLeft: "5%"
  },
  note: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    bottom: "10px",
    color: "#c0c1c2",
    display: "block",
    fontWeight: "400",
    fontSize: "13px",
    lineHeight: "13px",
    left: "0",
    marginLeft: "20px",
    position: "absolute",
    width: "260px"
  },
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  logoEPSEVG: {
    width: "50%"
  },
  license: {
    width: "150px"
  }
};

const useStyles = makeStyles(styles);

export default function About() {
  const classes = useStyles();

  const description = `
  CVentaur is a prototype platform where people with no expertise in artificial intelligence can 
  collaborate to create machine learning models oriented to object detection in images, with the 
  purpose of accelerating, improving and avoiding errors in this kind of tasks. It is aimed to take 
  advantage of the knowledge of these people and to digitise it so that it can be used and shared 
  more easily, while they are also benefitting.
  `
  return (
    <div>
      <Card>
        <CardHeader color="primary">
          <h4 className={classes.cardTitleWhite}>About</h4>
        </CardHeader>
        <CardBody>
          <div className={classes.step}>
            <p>{description}</p>
            <p>CVentaur is the result of the Bachelor's Thesis of <b>Jan Rodríguez Miret</b>, an Informatics Engineer student from <b>Escola Superior Politècnica de Vilanova i la Geltrú (EPSEVG)</b>.</p>
          </div>
          <img alt="EPSEVG Logo" className={classes.logoEPSEVG} src={require("../../assets/img/logo-epsevg.png")} />
          <div className={classes.step}>
            <p>The source code of the whole project is available <a href="https://bitbucket.org/janrodriguezmiret/cventaur/">here</a>.</p>
          </div>
        </CardBody>
      </Card>
      <Card>
        <CardHeader color="success">
          <h4 className={classes.cardTitleWhite}>Contact</h4>
        </CardHeader>
        <CardBody>
          <div className={classes.step}>
            <p>For any suggestion or doubt, please contact me on <a href="mailto:janrodriguezmiret@gmail.com">janrodriguezmiret@gmail.com</a>.</p>
          </div>
        </CardBody>
      </Card>
      <Card>
        <CardHeader color="info">
          <h4 className={classes.cardTitleWhite}>License</h4>
        </CardHeader>
        <CardBody>
          <div className={classes.step}>
            <p>This project its under a Creative Commons BY-NC-SA License. Find more details <a href="https://creativecommons.org/licenses/by-nc-sa/2.0/">here</a>.</p>
            <img alt="License" className={classes.license} src={require("../../assets/img/creative_commons.png")} />
          </div>
        </CardBody>
      </Card>
    </div>
  );
}
