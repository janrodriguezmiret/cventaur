import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Button from "components/CustomButtons/Button.js";
import { DialogTitle, DialogActions, DialogContent, InputLabel, MenuItem, FormHelperText, FormControl, Table, TableHead, TableRow, TableCell, TableBody } from "@material-ui/core";
import Add from "@material-ui/icons/Add";
import Dialog from '@material-ui/core/Dialog';
import Select from '@material-ui/core/Select'
import CustomInput from "components/CustomInput/CustomInput";
import TransferList from "components/TransferList/TransferList";
import { createCollection, getCollectionsByOwner, deleteCollection, getBaseCollections, getAllTags } from "../../api"
import { useAuth0 } from "../../react-auth0-spa"
import { useAsyncEffect } from "use-async-effect"
import { useGlobalState } from "../../globalstate"
import Icon from "@material-ui/core/Icon"
import Snackbar from "components/Snackbar/Snackbar";



const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  formControl: {
  },
  inputLabel: {
    marginTop: "45px"
  }
};

const useStyles = makeStyles(styles);

export default function MyCollections() {
  const { getTokenSilently, user, loading } = useAuth0()
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false)
  const [isOpenDelete, setIsOpenDelete] = useState(false)
  const [selectedCollection, setSelectedCollection] = useState()

  // States for a new collection
  const [name, setName] = useState("")
  const [description, setDescription] = useState("")
  const [basedOn, setBasedOn] = useState("")
  const [selectedTags, setSelectedTags] = useState([])
  // ----
  const [tags, setTags] = useState([])
  const [baseCollections, setBaseCollections] = useState([])
  const [collections, setCollections] = useState([]);
  const [state, dispatch] = useGlobalState()

  // Snackbar to inform successes or errors in operations
  const closeSnackbar = () => setOptionsSnackbar({ ...optionsSnackbar, open: false })
  const [optionsSnackbar, setOptionsSnackbar] = useState({ open: false, place: "bc", close: true, closeNotification: closeSnackbar })
  const showSnackbar = (success, message) => setOptionsSnackbar({ ...optionsSnackbar, open: true, color: success ? "success" : "danger", message: message })

  useAsyncEffect(async isMounted => refresh(isMounted), [loading, getTokenSilently, user, isOpen]);
  const refresh = async (isMounted) => {
    const token = await getTokenSilently()
    if (!loading) {
      try {
        const [responseCollections, responseModels, responseTags] = await Promise.all([
          getCollectionsByOwner(token, user.sub),
          getBaseCollections(token),
          getAllTags(token)])
        if (!isMounted) return;
        setCollections(responseCollections.data.data.collections)
        setBaseCollections(responseModels.data.data.baseCollections)
        setBasedOn(responseModels.data.data.baseCollections[0])
        setTags(responseTags.data.data.tags)
      }
      catch (error) {
        console.log(error)
        showSnackbar(false, error.message)
      }
    }
  }

  const toggleModal = () => {
    setIsOpen(!isOpen)
  }

  const toggleModalDelete = (collection = null) => {
    setSelectedCollection(collection)
    setIsOpenDelete(!isOpenDelete)
  }

  const handleName = (event) => {
    setName(event.target.value)
  }

  const handleDescription = (event) => {
    setDescription(event.target.value)
  }

  const handleBasedOn = (event) => {
    setBasedOn(event.target.value)
  }

  const handleTags = (tags) => {
    setSelectedTags(tags)
  }

  const newCollection = async () => {
    const token = await getTokenSilently()
    const collection = {
      name: name,
      description: description,
      tags: selectedTags.map(tag => tag._id),
      basedOn: basedOn,
      isBase: false,
    }

    try {
      const response = await createCollection(token, collection)
      dispatch({
        activeCollection: response.data.data.collection
      })
      showSnackbar(response.status === 201, response.data.message)
      setIsOpen(!isOpen)
    }
    catch (error) {
      console.log(error)
      showSnackbar(false, error.message)
    }
  }

  const removeCollection = async (collection_id) => {
    const token = await getTokenSilently()
    try {
      let response = await deleteCollection(token, collection_id)
      showSnackbar(response.status === 200, response.data.message)
      if (state.activeCollection && state.activeCollection._id === collection_id) {
        dispatch({ activeCollection: null })
      }
      setIsOpenDelete(false)
      refresh(true)
    }
    catch (error) {
      console.log(error)
      showSnackbar(false, error.message)
    }
  }

  var displayCollections = () => {
    if (!collections.length) {
      return <p>There aren't any collections yet.</p>
    }
    return collections.map((collection, key) => {
      const active = state.activeCollection && state.activeCollection._id === collection._id
      return (
        <TableRow
          key={key}
          style={{
            backgroundColor: active ? "lightgrey" : "white",
          }}
        >
          <TableCell>
            {/*eslint-disable-next-line*/}
            <Button color={active && "primary"} onClick={() => dispatch({ activeCollection: collection })}>
              {collection.name}
            </Button>
          </TableCell>
          <TableCell>
            {collection.tags.map(tag => tag.className).join(", ")}
          </TableCell>
          <TableCell>
            <Button
              justIcon
              key={key}
              color="danger"
              onClick={() => toggleModalDelete(collection._id)}>
              <Icon>delete</Icon>
            </Button>
          </TableCell>
        </TableRow>
      );
    })
  }

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Dialog open={isOpen} onClose={toggleModal} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Create a new collection</DialogTitle>
            <DialogContent>
              <CustomInput
                labelText="Name"
                id="name"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  onChange: handleName,
                  required: true,
                  autoFocus: true,
                }}
              />
              <CustomInput
                labelText="Description"
                id="description"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  onChange: handleDescription,
                  required: true,
                  multiline: true
                }}
              />
              <InputLabel className={classes.inputLabel}>Tags</InputLabel>
              <TransferList left={tags} handleRight={handleTags} />
              {baseCollections.length && (
                <FormControl fullWidth className={classes.inputLabel}>
                  <InputLabel id="based-on-label">Base model</InputLabel>
                  <Select
                    labelId="based-on-label"
                    id="based-on-select"
                    onChange={handleBasedOn}
                    value={basedOn}
                  >
                    {baseCollections.map((baseModel) => {
                      return (
                        <MenuItem key={baseModel._id} value={baseModel}>{baseModel.name}</MenuItem>
                      )
                    })}
                  </Select>
                  <FormHelperText>Choose a model already trained with images and classes similars to the ones you want</FormHelperText>
                </FormControl>
              )}
            </DialogContent>
            <DialogActions>
              <Button onClick={toggleModal} color="danger">
                Cancel
          </Button>
              <Button onClick={newCollection} color="success" disabled={!(name && description && basedOn && selectedTags.length)}>
                Create
          </Button>
            </DialogActions>
          </Dialog>
          <Dialog open={isOpenDelete} onClose={() => toggleModalDelete()} aria-labelledby="delete-dialog-title">
            <DialogTitle id="delete-dialog-title">Confirm validation</DialogTitle>
            <DialogContent>Are you sure you want to delete this collection? This action cannot be undone.</DialogContent>
            <DialogActions>
              <Button onClick={() => toggleModalDelete()}>
                Cancel
          </Button>
              <Button onClick={() => removeCollection(selectedCollection)} color="danger">
                Delete
          </Button>
            </DialogActions>
          </Dialog>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>My collections</h4>
            </CardHeader>
            <CardBody>
              <Table className={classes.table}>
                <TableHead>
                  <Button className={classes.right} type="button" color="primary" onClick={toggleModal}>
                    <Add />New collection
                  </Button>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Tags</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {displayCollections()}
                </TableBody>
              </Table>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
      <Snackbar {...optionsSnackbar} />
    </div >
  );
}
