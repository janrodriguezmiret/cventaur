import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { useAuth0 } from "../../react-auth0-spa"
import { useAsyncEffect } from "use-async-effect"
import { useGlobalState } from "../../globalstate"
import { TableBody, Table, TableHead, TableRow, TableCell, Tabs, Tab, Box } from "@material-ui/core";
import { getJobs } from "../../api"
import CardHeader from "components/Card/CardHeader";
import Card from "components/Card/Card";
import CardBody from "components/Card/CardBody";
import { NavLink } from "react-router-dom";
import Moment from "react-moment";
import GetApp from "@material-ui/icons/GetApp";
import Refresh from "@material-ui/icons/Refresh";
import Button from "components/CustomButtons/Button.js";
import { createExport, getExportsByCollection, getExportURL } from "../../api/index";
import ListIcon from "@material-ui/icons/List"
import Snackbar from "components/Snackbar/Snackbar";
import { primaryColor } from "assets/jss/material-dashboard-react";




const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  selectedRow: {
    backgroundColor: "#CCC"
  },
  tabs: {
    backgroundColor: "white", //"#9c27b0",
    textColor: "white"
  }
};

const useStyles = makeStyles(styles);

export default function Dataset() {
  const classes = useStyles();
  const { getTokenSilently, loading, user } = useAuth0()
  const [selectedJobs, setSelectedJobs] = useState([])
  const [allSelected, setAllSelected] = useState(false)
  const [jobs, setJobs] = useState([])
  const [exports_, setExports_] = useState([])
  const [tabIndex, setTabIndex] = useState(0)
  const [state] = useGlobalState()

  // Snackbar to inform successes or errors in operations
  const closeSnackbar = () => setOptionsSnackbar({ ...optionsSnackbar, open: false })
  const [optionsSnackbar, setOptionsSnackbar] = useState({ open: false, place: "bc", close: true, closeNotification: closeSnackbar })
  const showSnackbar = (success, message) => setOptionsSnackbar({ ...optionsSnackbar, open: true, color: success ? "success" : "danger", message: message })

  useAsyncEffect(async isMounted => refresh(isMounted), [loading, getTokenSilently]);

  const refresh = async (isMounted) => {
    const token = await getTokenSilently()
    if (!loading) {
      try {
        const [responseJobs, responseExports] = await Promise.all([
          getJobs(token, state.activeCollection._id, true),
          getExportsByCollection(token, state.activeCollection._id)
        ])
        if (!isMounted) return;
        const jobs = responseJobs.data.data.jobs
        const newExports = responseExports.data.data.exports
        setJobs(jobs)
        setExports_(newExports)
      }
      catch (error) {
        console.log(error)
        showSnackbar(false, error.message)
      }
    }
  }

  const toggleSelectOne = (index) => {
    const exists = selectedJobs.indexOf(index)
    // If it is already selected, unselect it
    if (exists > -1) {
      const newSelectedJobs = [...selectedJobs]
      newSelectedJobs.splice(exists, 1)
      setSelectedJobs(newSelectedJobs)
      if (allSelected) {
        setAllSelected(false)
      }
    }
    else {
      let newSelectedJobs = [...selectedJobs]
      newSelectedJobs.push(index)
      setSelectedJobs(newSelectedJobs)
    }

  }

  const toggleSelectAll = () => {
    if (!allSelected) {
      const allJobs = jobs.map((job, index) => index)
      setSelectedJobs(allJobs)
      setAllSelected(true)
    }
    else {
      setSelectedJobs([])
      setAllSelected(false)
    }
  }

  const createJobsExport = async () => {
    const token = await getTokenSilently()
    const jobIds = selectedJobs.map(index => jobs[index]._id)
    try {
      let response = await createExport(token, jobIds, state.activeCollection._id)
      showSnackbar(response.status === 201, response.data.message)
      setTabIndex(1)
      await refresh(true)
    }
    catch (error) {
      console.log(error)
      showSnackbar(false, error.message)
    }
  }

  const onChangeTab = async (event, newIndex) => {
    setTabIndex(newIndex)
    await refresh(true)
  }


  var displayJobs = () => {
    if (!jobs.length) {
      return <p>There are no predictions confirmed yet.</p>
    }
    return jobs.map((job, index) => {
      const creationDate = new Date(job.creationDate)
      const lastModified = new Date(job.lastModified)
      return (
        <TableRow style={selectedJobs.includes(index) ? styles.selectedRow : null} key={index}>
          <TableCell>
            {selectedJobs.includes(index) ?
              <input type="checkbox" checked={true} onClick={() => toggleSelectOne(index)} /> :
              <input type="checkbox" checked={false} onClick={() => toggleSelectOne(index)} />}
          </TableCell>
          <TableCell>
            <NavLink to={`/admin/predict/results/${job._id}`}>
              {job._id}
            </NavLink>
          </TableCell>
          <TableCell>
            <Moment format="DD/MM/YYYY - HH:mm">{creationDate}</Moment>
          </TableCell>
          <TableCell>
            {job.numTotal.toString()}
          </TableCell>
          <TableCell>
            <Moment format="DD/MM/YYYY - HH:mm">{lastModified}</Moment>{` (${user.nickname})`}
          </TableCell>
        </TableRow>
      );
    })
  }

  var displayExports = () => {
    if (!exports_.length) {
      return <p>There are no exports generated yet.</p>
    }
    return exports_.map((export_, index) => {
      const date = new Date(export_.creationDate)
      return (
        <TableRow key={index}>
          <TableCell>
            {export_.state === "processed" ?
              (<a href={getExportURL(export_._id + '.zip', state.activeCollection._id)}>
                {export_._id}
              </a>)
              : export_._id}
          </TableCell>
          <TableCell>
            <Moment format="DD/MM/YYYY - HH:mm">{date}</Moment>
          </TableCell>
          <TableCell>
            {export_.jobs.length}
          </TableCell>
          <TableCell>
            {export_.state}
          </TableCell>
        </TableRow>
      );
    })
  }

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            {/* eslint-disable-next-line*/}
            {children}
          </Box>
        )}
      </div>
    );
  }

  var summaryTab = (
    <Table className={classes.table}>
      <TableHead>
        <TableRow>
          <Button color="primary" onClick={createJobsExport} disabled={selectedJobs.length === 0}><GetApp />Export</Button>
        </TableRow>
        <TableRow>
          <TableCell><input type="checkbox" checked={allSelected} onClick={toggleSelectAll} /></TableCell>
          <TableCell>Id</TableCell>
          <TableCell>Created</TableCell>
          <TableCell>Total images</TableCell>
          <TableCell>Last modified</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {displayJobs()}
      </TableBody>
    </Table>
  )

  var exportsTab = (
    <Table className={classes.table}>
      <TableHead>
        <TableRow>
          <Button color="primary" onClick={refresh}><Refresh />Refresh</Button>
        </TableRow>
        <TableRow>
          <TableCell>Id</TableCell>
          <TableCell>Created</TableCell>
          <TableCell>Total predictions</TableCell>
          <TableCell>State</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {displayExports()}
      </TableBody>
    </Table>
  )

  return (
    <div>
      <Card>
        <CardHeader color="primary">
          <h4 className={classes.cardTitleWhite}>Predictions confirmed</h4>
        </CardHeader>
        <CardBody>
          <Tabs className={classes.tabs} TabIndicatorProps={{ style: { background: primaryColor[0] } }} value={tabIndex} onChange={onChangeTab} variant="fullWidth">
            <Tab label="Summary" icon={<ListIcon />} />
            <Tab label="Exports" icon={<GetApp />} />
          </Tabs>
          <TabPanel value={tabIndex} index={0}>
            {summaryTab}
          </TabPanel>
          <TabPanel value={tabIndex} index={1}>
            {exportsTab}
          </TabPanel>
        </CardBody>
      </Card>
      <Snackbar {...optionsSnackbar} />
    </div>

  );
}
