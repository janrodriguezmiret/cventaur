import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { InputLabel } from "@material-ui/core";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";

import { useGlobalState } from "../../globalstate";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  },
  field: {
    marginBottom: "30px",
  }
};

const useStyles = makeStyles(styles);

export default function Configuration() {
  const classes = useStyles();
  const [state] = useGlobalState()

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>{state.activeCollection.name}</h4>
              <p className={classes.cardCategoryWhite}>Details</p>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={5}>
                  <InputLabel>Collection name</InputLabel>
                  <p className={classes.field}>{state.activeCollection.name}</p>
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <InputLabel>Description</InputLabel>
                  <p className={classes.field}>{state.activeCollection.description}</p>
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <InputLabel>Tags</InputLabel>
                  <p>{state.activeCollection.tags && state.activeCollection.tags.map(tag => tag.className).join(", ")}</p>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Manage users and permissions</h4>
              <p className={classes.cardCategoryWhite}>Share this collection with others</p>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={5}>
                  <InputLabel>Owner</InputLabel>
                  <p className={classes.field}>{state.activeCollection.owner}</p>
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <InputLabel>Other users:</InputLabel>
                  <p className={classes.field}>It seems there aren't any other user that can acces this collection</p>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
