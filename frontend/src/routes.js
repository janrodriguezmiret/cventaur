/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Assessment from "@material-ui/icons/Assessment";
import Settings from "@material-ui/icons/Settings";
import Visibility from "@material-ui/icons/Visibility";
import Search from "@material-ui/icons/Search";
import Collections from "@material-ui/icons/Collections";
import CheckCircle from "@material-ui/icons/CheckCircle";
import Build from "@material-ui/icons/Build";
import Help from "@material-ui/icons/Help";
import Info from "@material-ui/icons/Info";

import MyCollections from "views/MyCollections/MyCollections";
import PublicCollections from "views/PublicCollections/PublicCollections";
import DashboardPage from "views/Dashboard/Dashboard.js";
import Predict from "views/Predict/Predict";
import Dataset from "views/Dataset/Dataset";
import Result from "views/Result/Result";
import AIBuilder from "views/AIBuilder/AIBuilder";
import About from "views/About/About";
import HelpPage from "views/Help/Help";
import Configuration from "views/Configuration/Configuration";


export const routes = [
  {
    path: "/my-collections",
    name: "My collections",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Dashboard,
    component: MyCollections,
    layout: "/admin",
  },
  {
    path: "/public",
    name: "Public collections",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Search,
    component: PublicCollections,
    layout: "/admin",
  },
];

export const belowRoutes = [
  {
    path: "/about",
    name: "About",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Info,
    component: About,
    layout: "/admin",
  },
  {
    path: "/help",
    name: "Help",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Help,
    component: HelpPage,
    layout: "/admin",
  },
];

export const collectionRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Assessment,
    component: DashboardPage,
    layout: "/admin",
  },
  {
    path: "/predict/results/:jobId",
    name: "Results",
    icon: CheckCircle,
    component: Result,
    layout: "/admin",
    hidden: true,
  },
  {
    path: "/predict",
    name: "Predict",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Visibility,
    component: Predict,
    layout: "/admin",
  },
  {
    path: "/dataset",
    name: "Dataset",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Collections,
    component: Dataset,
    layout: "/admin",
  },
  {
    path: "/aibuilder",
    name: "AI Builder",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Build,
    component: AIBuilder,
    layout: "/admin",
  },
  {
    path: "/configuration",
    name: "Configuration",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Settings,
    component: Configuration,
    layout: "/admin",
  },
];