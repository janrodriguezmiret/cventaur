import React, { useRef, useState } from "react";
import Button from "components/CustomButtons/Button.js";
import { NavLink } from "react-router-dom";
import { AppBar, Toolbar, makeStyles } from "@material-ui/core";
import homeImage from "../assets/img/home_image.png";
import predictImage from "../assets/img/predict.png";
import feedbackImage from "../assets/img/feedback.png";
import dashboardImage from "../assets/img/dashboard.png";
import logo from "../assets/img/icon_white.png";
import Card from "components/Card/Card";
import CardHeader from "components/Card/CardHeader";
import CardBody from "components/Card/CardBody";
import Footer from "components/Footer/Footer";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Autorenew from "@material-ui/icons/AutorenewRounded";
import FastForward from "@material-ui/icons/FastForwardRounded";
import TrendingUp from "@material-ui/icons/TrendingUpRounded";
import {
  successColor,
  warningColor,
  dangerColor
} from "../assets/jss/material-dashboard-react";
import Snackbar from "components/Snackbar/Snackbar";

const styles = {
  toolbar: {
    backgroundColor: "#303030",
    position: "relative",
  },
  toolbarRight: {
    position: "absolute",
    right: "0",
    marginRight: "20px",
  },
  toolbarCenter: {
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)",
  },
  centerImage: {
    position: "absolute",
    width: "60%",
    padding: "30px 5px 30px 5px",
    top: "50%",
    left: "50%",
    textAlign: "center",
    transform: "translate(-50%, -50%)",
    backgroundColor: "black",
    opacity: "0.8",
    borderRadius: "15%"
  },
  container: {
    position: "relative",
    textAlign: "center",
  },
  homeImage: {
    width: "100%",
    opacity: "0.4"
  },
  detailImage: {
    maxHeight: "700px",
    width: "80%",
    opacity: "0.6"
  },
  detail: {
    alignItems: "center",
    paddingBottom: "60px",
  },
  detailNote: {
    fontSize: 16,
  },
  iconContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
  },
  detailIcon: {
    fontSize: 75,
  },
  title: {
    fontSize: "4vw",
    color: "white"
  },
  subtitle: {
    fontSize: "2vw",
    color: "#B0B0B0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  logoEPSEVG: {
    width: "50%"
  },
  license: {
    width: "150px"
  },
};

const useStyles = makeStyles(styles);

export default function Home() {
  const classes = useStyles();
  const refHow = useRef()

  const scrollToRef = (ref) => ref.current.scrollIntoView({ behavior: 'smooth' })

  // Snackbar to inform successes or errors in operations
  const browserSupport = `
  Google Chrome is currently the only browser supported.
  You may experience network errors if you use another.
  `
  const closeSnackbar = () => setOptionsSnackbar({ ...optionsSnackbar, open: false })
  const [optionsSnackbar, setOptionsSnackbar] = useState({ open: true, place: "bc", close: true, color: "danger", closeNotification: closeSnackbar, message: browserSupport })
  
  return (
    <div>
      <AppBar>
        <Toolbar className={classes.toolbar}>
          <img alt="" src={logo} />
          <p>CVentaur</p>
          <div className={classes.toolbarRight}>
            <NavLink to="/admin/my-collections">
              <Button type="button" round color="primary">
                Enter site
              </Button>
            </NavLink>
          </div>
        </Toolbar>
      </AppBar>
      <div className={classes.container}>
        <img alt="" className={classes.homeImage} src={homeImage} />
        <div className={classes.centerImage}>
          <h1 className={classes.title}>CVentaur</h1>
          <h2 className={classes.subtitle}>An object detection tool that learns from you</h2>
          <Button style={{ marginRight: 10, opacity: 1 }} round type="button" color="info" onClick={() => scrollToRef(refHow)}>
            How does it work?
          </Button>
          <NavLink to="/admin/my-collections">
            <Button style={{ marginLeft: 10, opacity: 1 }} round type="button" color="primary">
              Enter site
            </Button>
          </NavLink>
        </div>
        <Snackbar {...optionsSnackbar} />
      </div>
      <div ref={refHow} style={{ marginBottom: 100 }}>
        <p />
      </div>
      <div style={{ margin: "0px 20px 0px 20px" }}>
        <GridContainer style={styles.detail}>
          <GridItem xs={12} sm={12} md={5}>
            <Card>
              <CardHeader color="success">
                <h4 className={classes.cardTitleWhite}>Easy to understand, fast to use</h4>
              </CardHeader>
              <CardBody>
                <p className={classes.detailNote}>Start labeling in just a <b>few seconds.</b></p>
                <p className={classes.detailNote}>Create a collection and specify the <b>kind of objects</b> you want to detect.</p>
                <div className={classes.iconContainer}>
                  <FastForward className={classes.detailIcon} style={{ color: successColor[0] }} />
                </div>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={7}>
            <img alt="" className={classes.detailImage} src={predictImage} />
          </GridItem>
        </GridContainer>
        <GridContainer style={styles.detail}>
          <GridItem xs={12} sm={12} md={7}>
            <img alt="" className={classes.detailImage} src={feedbackImage} />
          </GridItem>
          <GridItem xs={12} sm={12} md={5}>
            <Card>
              <CardHeader color="warning">
                <h4 className={classes.cardTitleWhite}>Save time and resources</h4>
              </CardHeader>
              <CardBody>
                <p className={classes.detailNote}>Upload images and let the AI to provide you with its <b>predictions.</b></p>
                <p className={classes.detailNote}>Work <b>together</b> and help improve each other.</p>
                <div className={classes.iconContainer}>
                  <TrendingUp className={classes.detailIcon} style={{ color: warningColor[0] }} />
                </div>
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer style={styles.detail}>
          <GridItem xs={12} sm={12} md={5}>
            <Card>
              <CardHeader color="danger">
                <h4 className={classes.cardTitleWhite}>The more you use it, the more reliable it becomes</h4>
              </CardHeader>
              <CardBody>
                <p className={classes.detailNote}>Give it <b>feedback</b> and <b>train</b> it so it can improve over time.</p>
                <p className={classes.detailNote}><b>Share</b> it with collaborators to increase the speed of learning.</p>
                <div className={classes.iconContainer}>
                  <Autorenew className={classes.detailIcon} style={{ color: dangerColor[0] }} />
                </div>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={7}>
            <img alt="" className={classes.detailImage} src={dashboardImage} />
          </GridItem>
        </GridContainer>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>About</h4>
              </CardHeader>
              <CardBody>
                <div className={classes.step}>
                  <p className={classes.detailNote}>CVentaur is the result of the Bachelor's Thesis of Jan Rodríguez Miret, an Informatics Engineer student from Escola Superior Politècnica de Vilanova i la Geltrú (EPSEVG).</p>
                  <img alt="EPSEVG Logo" className={classes.logoEPSEVG} src={require("../assets/img/logo-epsevg.png")} />
                </div>
                <div className={classes.step}>
                  <p className={classes.detailNote}>The source code of the whole project is available <a href="https://bitbucket.org/janrodriguezmiret/cventaur/">here</a>.</p>
                </div>
                <div className={classes.step}>
                  <p className={classes.detailNote}>For any suggestion or doubt, please contact me on <a href="mailto:janrodriguezmiret@gmail.com">janrodriguezmiret@gmail.com</a>.</p>
                </div>
              </CardBody>
            </Card>
          </GridItem>
          <Card>
            <CardHeader color="info">
              <h4 className={classes.cardTitleWhite}>License</h4>
            </CardHeader>
            <CardBody>
              <div className={classes.step}>
                <p>This project its under a Creative Commons BY-NC-SA License. Find more details <a href="https://creativecommons.org/licenses/by-nc-sa/2.0/">here</a>.</p>
                <img alt="License" className={classes.license} src={require("../assets/img/creative_commons.png")} />
              </div>
            </CardBody>
          </Card>
        </GridContainer>
      </div>
      <Footer />
    </div >
  );
}
