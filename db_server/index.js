const express = require("express");
const bodyParser = require('body-parser')
const collectionRouter = require("./routers/collection_router")
const userRouter = require("./routers/user_router")
const tagRouter = require("./routers/tag_router")
const jobRouter = require("./routers/job_router")
const validationRouter = require("./routers/validation_router")
const exportRouter = require("./routers/export_router")
const modelRouter = require("./routers/model_router")


require('./db')

// Create a new Express app
const app = express();


app.use(bodyParser.json());

app.use('/api/users', userRouter)
app.use('/api/collections', collectionRouter)
app.use('/api/tags', tagRouter)
app.use('/api/jobs', jobRouter)
app.use('/api/validations', validationRouter)
app.use('/api/exports', exportRouter)
app.use('/api/models', modelRouter)

// Start the app
app.listen(8080, () => console.log('API listening on 8080'));