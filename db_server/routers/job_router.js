const express = require('express')
const jobCtrl = require("../controllers/job")

const router = express.Router()

router.get('/next', jobCtrl.getNextJobs)
router.patch('/:jobId', jobCtrl.updateJob)

module.exports = router