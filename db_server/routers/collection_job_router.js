const express = require('express')
const jobCtrl = require("../controllers/job")

const router = express.Router()

router.post('', jobCtrl.createJob)
router.get('', jobCtrl.getJobs)

module.exports = router