const express = require('express')

const UserCtrl = require('../controllers/user')

const publicRouter = express.Router()

publicRouter.post('/signup', UserCtrl.createUser)
publicRouter.post('/authenticate', UserCtrl.authenticate)

module.exports = publicRouter