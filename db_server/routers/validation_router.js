const express = require('express')
const validationCtrl = require("../controllers/validation")

const router = express.Router()

router.post('/:validationId/prediction', validationCtrl.makePrediction)
router.get('/job/:jobId', validationCtrl.getValidations)
router.patch('/job/:jobId', validationCtrl.updateJobValidations)


module.exports = router