const express = require('express')
const exportCtrl = require("../controllers/export")

const router = express.Router()

router.get('/next', exportCtrl.getNextExport)
router.post('', exportCtrl.createExport)
router.get('/collection/:collectionId', exportCtrl.getExportsByCollection)
router.patch('/:exportId', exportCtrl.updateExport)
router.get('/:exportId', exportCtrl.getExport)

module.exports = router