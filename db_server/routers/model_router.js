const express = require('express')

const modelCtrl = require('../controllers/model')

const router = express.Router()

router.post('/collection/:collectionId', modelCtrl.createModel)

module.exports = router