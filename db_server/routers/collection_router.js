const express = require('express')
const CollectionCtrl = require("../controllers/collection")
const collectionJobRouter = require("../routers/collection_job_router")
const router = express.Router()

router.post('', CollectionCtrl.createCollection)
router.get('/owner/:owner_id', CollectionCtrl.getCollectionsByOwner)
router.delete('/:collection_id', CollectionCtrl.deleteCollection)
router.get('/base', CollectionCtrl.getBaseCollections)
router.use('/:collection_id/jobs', function (req, res, next) {
  req.collection_id = req.params.collection_id
  next()
}, collectionJobRouter)

module.exports = router