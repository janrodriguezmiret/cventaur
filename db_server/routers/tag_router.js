const express = require('express')
const tagCtrl = require("../controllers/tag")

const router = express.Router()

router.get('', tagCtrl.getAllTags)
router.post('', tagCtrl.createTag)

module.exports = router