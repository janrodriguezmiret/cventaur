const express = require('express')

const UserCtrl = require('../controllers/user')

const router = express.Router()

router.post('/:id', UserCtrl.createUser)

module.exports = router