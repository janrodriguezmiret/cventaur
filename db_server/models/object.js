const mongoose = require('mongoose')
const idvalidator = require('mongoose-id-validator');
const Schema = mongoose.Schema

const objectSchema = new Schema(
  {
    bbox: { type: [Number], required: true, validate: arrayLimit },
    prob: { type: Schema.Types.Number, required: true },
    tag: { type: Schema.Types.ObjectId, ref: 'Tag', required: true }
  },
)

function arrayLimit(val) {
  return val.length === 4
}

objectSchema.plugin(idvalidator);
module.exports = mongoose.model('Object', objectSchema)