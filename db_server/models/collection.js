const mongoose = require('mongoose')
const idvalidator = require('mongoose-id-validator');
const Schema = mongoose.Schema

const collectionSchema = new Schema(
    {
        name: { type: String, required: true },
        description: { type: String, required: true },
        owner: { type: String, required: true },
        basedOn: { type: Schema.Types.ObjectId, ref: 'Collection', default: null },
        tags: [{ type: Schema.Types.ObjectId, ref: 'Tag' }],
        isBase: { type: Boolean, required: true }
    },
)
collectionSchema.plugin(idvalidator);

module.exports = mongoose.model('Collection', collectionSchema)