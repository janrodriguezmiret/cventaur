const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema(
    {
        //We want to override the default _id field
        _id: { type: String, required: true },
    },
    {
        _id: false
    })


module.exports = mongoose.model('User', userSchema)