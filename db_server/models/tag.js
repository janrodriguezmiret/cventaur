const mongoose = require('mongoose')
const Schema = mongoose.Schema

const tagSchema = new Schema(
    {
        className: { type: String, required: true, lowercase: true },
    },
)

module.exports = mongoose.model('Tag', tagSchema)