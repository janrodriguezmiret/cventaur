const mongoose = require('mongoose')
const Schema = mongoose.Schema

const imageSchema = new Schema(
  {
    filename: { type: String, required: true },
    width: { type: Number, required: true},
    height: { type: Number, required: true},
  }
)

module.exports = mongoose.model('Image', imageSchema)