const mongoose = require('mongoose')
const Schema = mongoose.Schema

const exportSchema = new Schema(
  {
    _collection: { type: Schema.Types.ObjectId, ref: 'Collection', required: true },
    jobs: { type: [{ type: Schema.Types.ObjectId, ref: 'Job' }], required: true },
    state: { type: String, required: true, enum: ['waiting','processing', 'processed'] },
    creationDate: { type: Date, required: true, default: new Date()},
  }
)

module.exports = mongoose.model('Export', exportSchema)