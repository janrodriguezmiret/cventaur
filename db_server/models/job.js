const mongoose = require('mongoose')
const idvalidator = require('mongoose-id-validator');
const Schema = mongoose.Schema

const jobSchema = new Schema(
  {
    _collection: { type: Schema.Types.ObjectId, ref: 'Collection', required: true },
    state: { type: String, required: true, enum: ['waiting', 'processing', 'processed', 'confirmed'] },
    creationDate: { type: Date, required: true, default: new Date() },
    lastModified: { type: Date, required: true, default: new Date() },
    lastExport: { type: Date, required: false },
    hasPrediction: { type: Boolean, required: true },
    numTotal: { type: Number, required: true, default: 0 },
    numConfirmed: { type: Number, required: true, default: 0 },
    numPredicted: { type: Number, required: false, default: 0 },
  },
)
jobSchema.plugin(idvalidator);
module.exports = mongoose.model('Job', jobSchema)