const mongoose = require('mongoose')
const idvalidator = require('mongoose-id-validator');
const Schema = mongoose.Schema

const predictionSchema = new Schema(
  {
    objects: { type: [{ type: Schema.Types.ObjectId, ref: 'Object' }], required: true }
  },
)

predictionSchema.plugin(idvalidator);
module.exports = mongoose.model('Prediction', predictionSchema)