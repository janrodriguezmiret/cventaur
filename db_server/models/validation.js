const mongoose = require('mongoose')
const idvalidator = require('mongoose-id-validator');
const Schema = mongoose.Schema

const validationSchema = new Schema(
  {
    job: { type: Schema.Types.ObjectId, ref: 'Job', required: true },
    image: { type: Schema.Types.ObjectId, ref: 'Image', required: true },
    confirmed: { type: Boolean, required: true, default: false },
    prediction: { type: Schema.Types.ObjectId, ref: 'Prediction', required: false },
    objects: { type: [{ type: Schema.Types.ObjectId, ref: 'Object' }], required: true }
  }
)

validationSchema.plugin(idvalidator);
module.exports = mongoose.model('Validation', validationSchema)