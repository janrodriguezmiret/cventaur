const Collection = require("../models/collection")
const Validation = require("../models/validation")
const Job = require("../models/job")
const Image = require("../models/image")
const ObjectModel = require("../models/object")
const db = require("../db/index")

const createJob = async (req, res) => {
  const sub = req.body.sub
  const collection_id = req.collection_id
  const imgProps = req.body.imgProps
  if (typeof sub !== "string" || !Array.isArray(imgProps)) {
    return res.status(400).json({
      succeed: false,
      message: "Sub or imgProps not given correctly"
    })
  }
  let session = null
  try {
    let collection = await Collection.findById(collection_id).exec()
    if (!collection) {
      return res.status(400).json({
        success: false,
        message: "Collection does not exist"
      })
    }
    if (sub !== collection.owner) {
      return res.status(403).json({
        success: false,
        message: "Forbidden"
      })
    }
    await Promise.all([Job.createCollection(), Image.createCollection(), Validation.createCollection()])
    session = await db.startSession()
    session.startTransaction()

    let job = new Job({
      _collection: collection_id,
      state: "waiting",
      hasPrediction: true,
      numTotal: imgProps.length,
      numConfirmed: 0,
      numPredicted: 0,
    })
    let jobDoc = await job.save({ session: session })
    let images = imgProps.map(imgProp => {
      const image = new Image({
        width: imgProp.width,
        height: imgProp.height
      })
      image.filename = image._id + "." + imgProp.extension
      return image
    })
    let imagesDocs = await Image.insertMany(images, { session: session })
    let validations = []
    imagesDocs.forEach(image => {
      validations.push(new Validation({
        _id: image._id,
        job: jobDoc._id,
        image: image._id,
        confirmed: false,
        objects: [],
      }))
    })
    //await Validation.createCollection()
    await Validation.insertMany(validations, { session: session })
    await session.commitTransaction()
    return res.status(201).json({
      succeed: true,
      message: "Job created successfully",
      data: {
        job: job,
        imagesIds: imagesDocs.map(imageDoc => imageDoc._id),
      }
    })
  }
  catch (error) {
    if (session != null) {
      await session.abortTransaction()
    }
    console.log(error)
    return res.status(500).json({
      error,
      message: 'Cannot get collections',
    })
  }

}

const getJobs = async (req, res) => {
  const sub = req.query.sub
  const collection_id = req.collection_id
  const confirmed = req.query.confirmed
  if (typeof sub !== "string" || typeof confirmed !== "string") {
    return res.status(400).json({
      succeed: false,
      message: "Sub or confirmed not given"
    })
  }
  try {
    let collection = await Collection.findById(collection_id).exec()
    if (!collection) {
      return res.status(400).json({
        success: false,
        message: "Collection does not exist"
      })
    }
    if (sub !== collection.owner) {
      return res.status(403).json({
        success: false,
        message: "Forbidden"
      })
    }
    let queryStates = confirmed == "true" ? ["confirmed"] : ["waiting", "processing", "processed"]
    let jobs = await Job.find({ _collection: collection_id, state: { $in: queryStates } }).sort({creationDate: 'desc' }).exec()
    return res.status(200).json({
      succeed: true,
      message: "Jobs successfully retrieved",
      data: { jobs: jobs }
    })
  }
  catch (error) {
    console.log(error)
    return res.status(500).json({
      error,
      message: 'Cannot get jobs',
    })
  }
}

const getNextJobs = async (req, res) => {
  const numJobsString = req.query.numJobs
  const numJobs = parseInt(numJobsString)
  if (!numJobsString || typeof numJobs !== "number") {
    return res.status(400).json({
      succeed: false,
      message: "numJobs not given correctly"
    })
  }
  try {
    const jobs = await Job.find({ hasPrediction: true, state: "waiting" }).sort({ "creationDate": 1 }).limit(numJobs).exec()
    return res.status(200).json({
      succeed: true,
      message: "Jobs successfully retrieved",
      data: { jobs: jobs }
    })
  }
  catch (error) {
    console.log(error)
    return res.status(500).json({
      error,
      message: 'Cannot get jobs',
    })
  }
}

const updateJob = async (req, res) => {
  const jobChanges = req.body.jobChanges
  const sub = req.body.sub
  const jobId = req.params.jobId
  if (typeof jobChanges !== "object") {
    return res.status(400).json({
      succeed: false,
      message: "Job not given correctly"
    })
  }
  let session = null
  try {
    let jobDoc = await Job.findById(jobId).populate("_collection", "owner").exec()
    if (!jobDoc) {
      return res.status(400).json({
        success: false,
        message: "Job does not exist"
      })
    }
    if (sub && sub !== jobDoc._collection.owner) {
      return res.status(403).json({
        success: false,
        message: "Forbidden"
      })
    }
    session = await db.startSession()
    session.startTransaction()
    // Update job fields
    let isNowConfirmed = false
    Object.keys(jobChanges).forEach(key => {
      // If job is now confirmed, update its validations and percentage of the objects
      if (jobDoc.state === "processed" && key === "state" && jobChanges[key] === "confirmed") {
        isNowConfirmed = true
      }
      jobDoc.set(key, jobChanges[key])
    });
    if (isNowConfirmed) {
      let validations = await Validation.find({ job: jobId }).exec()
      const promises = validations.map(async (validation) => {
        await ObjectModel.updateMany({ _id: { $in: validation.objects } }, { $set: { prob: 100 } }, { session: session }).exec()
        await validation.updateOne({ $set: { confirmed: true } }, { session: session }).exec()
        jobDoc.numConfirmed = jobDoc.numTotal
      })
      await Promise.all(promises)
    }
    await jobDoc.save({ session: session });
    await session.commitTransaction()
    return res.status(200).json({
      succeed: true,
      message: "Job updated successfully"
    })
  }
  catch (error) {
    if (session !== null) {
      await session.abortTransaction()
    }
    console.log(error)
    return res.status(500).json({
      succeed: false,
      message: "Internal server error"
    })
  }
}

module.exports = {
  createJob,
  getJobs,
  getNextJobs,
  updateJob
}