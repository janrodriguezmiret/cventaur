const Validation = require("../models/validation")
const Prediction = require("../models/prediction")
const Tag = require("../models/tag")
const Object = require("../models/object")
const Job = require("../models/job")
const db = require("../db/index")


const makePrediction = async (req, res) => {
  const validationId = req.params.validationId
  const objects = req.body.objects
  if (!Array.isArray(objects)) {
    return res.status(400).json({
      succeed: false,
      message: "Objects not given correctly",
    })
  }
  let session = null
  try {
    await Promise.all([Prediction.createCollection(), Object.createCollection()])
    session = await db.startSession()
    session.startTransaction()
    // Create Objects
    let createObjects = objects.map(async (object) => {
      const tag = await Tag.findOne({ className: object.name }).exec()
      return new Object({ tag: tag._id, prob: object.prob, bbox: object.bbox })
    })
    const objectsDocs = await Promise.all(createObjects)
    await Object.insertMany(objectsDocs, { session: session })

    // Create Prediction
    const objectsIds = objectsDocs.map(objectDoc => objectDoc._id)
    const prediction = new Prediction({ _id: validationId, objects: objectsIds })
    await prediction.save({ session: session })

    // Update Validation
    let validation = await Validation.findById(validationId).exec()
    validation.prediction = prediction._id
    validation.objects = objectsIds
    await validation.save({ session: session })

    // Update Job
    let job = await Job.findById(validation.job)
    job.numPredicted = job.numPredicted + 1
    await job.save({ session: session })

    await session.commitTransaction()
    return res.status(200).json({
      succeed: true,
      message: "Prediction made successfully",
    })
  }
  catch (error) {
    console.log(error)
    if (session != null) {
      await session.abortTransaction()
    }
    return res.status(500).json({
      succeed: false,
      message: 'Internal server error',
    })
  }
}

const getValidations = async (req, res) => {
  const jobId = req.params.jobId
  const sub = req.query.sub
  if (typeof sub !== "string") {
    return res.status(400).json({
      succeed: false,
      message: "Sub not given correctly",
    })
  }
  try {
    const job = await Job.findById(jobId).populate("_collection", "owner").exec()
    if (!job) {
      return res.status(400).json({
        succeed: false,
        message: "Job doesn't exist",
      })
    }
    if (job._collection.owner !== sub) {
      return res.status(403).json({
        succeed: false,
        message: "You don't have permission",
      })
    }
    const validations = await Validation.find({ job: job._id })
      .populate({
        path: "objects",
        populate: { path: "tag" }
      })
      .populate("image")
      .exec()
    return res.status(200).json({
      succeed: true,
      message: "Validations retrieved successfully",
      data: { validations, job }
    })
  }
  catch (error) {
    console.log(error)
    return res.status(500).json({
      succeed: false,
      message: 'Internal server error',
    })
  }
}

const updateJobValidations = async (req, res) => {
  const jobId = req.params.jobId
  const sub = req.body.sub
  const validations = req.body.validations
  if (typeof sub !== "string") {
    return res.status(400).json({
      succeed: false,
      message: "Sub not given correctly",
    })
  }
  let session = null
  try {
    const job = await Job.findById(jobId).populate("_collection").exec()
    if (!job) {
      return res.status(400).json({
        succeed: false,
        message: "Job doesn't exist",
      })
    }
    if (job._collection.owner !== sub) {
      return res.status(403).json({
        succeed: false,
        message: "You don't have permission",
      })
    }
    session = await db.startSession()
    session.startTransaction()
    // Delete current objects of the validations and create new ones
    const validationDocs = await Validation.find({ job: job._id }).exec()
    let modifiedIds = []
    // TODO: check if ids from the validations match the ones of validationDocs, to ensure all validations of the job are updated
    const deleteObjectsPromises = validationDocs.map(async (validationDoc) => {
      modifiedIds.push(validationDoc.objects)
      await Object.deleteMany({ _id: { $in: validationDoc.objects } }, { session: session })
      const objects = validations[validationDoc._id]
      const objectDocs = await Object.insertMany(objects, { session: session })
      const objectIds = objectDocs.map(objectDoc => objectDoc._id)
      modifiedIds.push(objectIds)
      validationDoc.set("objects", objectIds)
      modifiedIds.push(validationDoc._id)
      await validationDoc.save({ session: session })
    })
    await Promise.all(deleteObjectsPromises)
    job.lastModified = new Date()
    await job.save({ session: session })
    await session.commitTransaction()
    return res.status(200).json({
      succeed: true,
      message: "Validations updated successfully",
    })
  }
  catch (error) {
    if (session != null) {
      await session.abortTransaction()
    }
    console.log(error)
    return res.status(500).json({
      succeed: false,
      message: 'Internal server error',
    })
  }
}

module.exports = {
  makePrediction,
  getValidations,
  updateJobValidations,
}
