const Collection = require('../models/collection')

const createModel = async (req, res) => {
  const collectionId = req.params.collectionId
  const sub = req.body.sub

  if (typeof sub !== "string") {
    return res.status(400).json({
      success: false,
      message: "Sub or collectionId not given"
    })
  }
  try {
    const collectionDoc = await Collection.findById(collectionId).populate("tags","className").exec()
    if (!collectionDoc) {
      return res.status(400).json({
        success: false,
        message: "Collection does not exist"
      })
    }
    if (sub !== collectionDoc.owner) {
      return res.status(403).json({
        success: false,
        message: "Forbidden"
      })
    }
    return res.status(201).json({
      success: true,
      message: 'Model created successfully',
      data: { collection: collectionDoc },
    })
  }
  catch (error) {
    console.log(error)
    return res.status(500).json({
      success: false,
      error,
      message: 'User not created',
    })
  }
}

module.exports = {
  createModel,
}