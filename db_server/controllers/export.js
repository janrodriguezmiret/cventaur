const Export = require("../models/export")
const Job = require("../models/job")
const Validation = require("../models/validation")
const Collection = require("../models//collection")

const db = require("../db/index")

const getExportsByCollection = async (req, res) => {
  const collectionId = req.params.collectionId
  const sub = req.body.sub
  if (typeof sub !== "string") {
    return res.status(400).json({
      success: false,
      message: "Sub not given correctly",
    })
  }
  try {
    let collectionDoc = await Collection.findById(collectionId).exec()
    if (collectionDoc.owner !== sub) {
      return res.status(403).json({
        success: false,
        message: "You don't have permission",
        error: "Forbidden"
      })
    }
    let exportDocs = await Export.find({ _collection: collectionId }).sort({ creationDate: 'desc' }).exec()
    return res.status(200).json({
      success: true,
      message: "Exports retrieved successfully",
      data: { exports: exportDocs },
    })
  }
  catch (error) {
    console.log(error)
    return res.status(500).json({
      error: error.message,
      message: 'Cannot get Exports',
      success: false
    })
  }
}

const createExport = async (req, res) => {
  const { jobIds, collectionId } = req.body
  try {
    if (!Array.isArray(jobIds) || !collectionId) {
      return res.status(400).json({
        success: false,
        message: "jobIds or collectionId not given",
      })
    }
    // TODO: ensure all jobs from the same collection
    const exportDoc = new Export({ _collection: collectionId, jobs: jobIds, state: "waiting", creationDate: new Date() })
    await exportDoc.save()
    return res.status(201).json({
      success: true,
      message: 'Export created successfully',
      data: { export: exportDoc }
    })
  }
  catch (error) {
    if (error.name === "ValidationError") {
      return res.status(400).json({
        success: false,
        message: 'Data not valid',
        error: error.message
      })
    }
    console.log(error)
    return res.status(500).json({
      error: error.message,
      message: 'Cannot create export',
      success: false
    })
  }
}

const getNextExport = async (req, res) => {
  try {
    // Get next exports
    const exportDoc = await Export.findOne({ state: "waiting" }).sort({ "creationDate": 1 }).populate("jobs").exec()
    if (!exportDoc) {
      return res.status(200).json({
        succeed: true,
        message: "Exports successfully retrieved",
        data: { exports: [] }
      })
    }
    // Iterate over its jobs to include the validation JSON
    let newExport = { ...exportDoc._doc }
    let jobs = Array.from(exportDoc.jobs)
    const promisesJobs = jobs.map(async (job) => {
      // Return Validations only if they were not created or are not valid
      let newJob = { ...job._doc }
      if (!job.lastExport || job.lastModified > job.lastExport) {
        const validations = await Validation.find({ job: job._id }).select(["objects"])
          .populate({
            path: "objects",
            select: "-_id -__v",
            populate: { path: "tag", select: "-_id -__v" }
          })
          .populate({ path: "image", select: "-_id -__v" })
          .exec()
        newJob["validations"] = validations
      }
      return newJob
    })
    jobs = await Promise.all(promisesJobs)
    newExport.jobs = jobs
    return res.status(200).json({
      succeed: true,
      message: "Exports successfully retrieved",
      data: { exports: [newExport] }
    })
  }
  catch (error) {
    console.log(error)
    return res.status(500).json({
      error,
      message: 'Cannot get Exports',
    })
  }
}

const updateExport = async (req, res) => {
  const exportChanges = req.body.exportChanges
  const exportId = req.params.exportId
  if (typeof exportChanges !== "object") {
    return res.status(400).json({
      succeed: false,
      message: "Export not given correctly"
    })
  }
  let session = null
  try {
    let exportDoc = await Export.findById(exportId).populate("_collection", "owner").exec()
    if (!exportDoc) {
      return res.status(400).json({
        success: false,
        message: "Export does not exist"
      })
    }
    session = await db.startSession()
    session.startTransaction()
    // Update job fields
    let isNowProcessed = false
    Object.keys(exportChanges).forEach(key => {
      // If export has finished, update all of its Job's lastExport
      if (exportDoc.state === "processing" && key === "state" && exportChanges[key] === "processed") {
        isNowProcessed = true
      }
      exportDoc.set(key, exportChanges[key])
    });
    if (isNowProcessed) {
      await Job.updateMany({ _id: { $in: exportDoc.jobs } }, { $set: { lastExport: new Date() } }, { session: session }).exec()
    }
    await exportDoc.save({ session: session });
    await session.commitTransaction()
    return res.status(200).json({
      succeed: true,
      message: "Export updated successfully"
    })
  }
  catch (error) {
    if (session !== null) {
      await session.abortTransaction()
    }
    console.log(error)
    return res.status(500).json({
      succeed: false,
      message: "Internal server error"
    })
  }
}

const getExport = async (req, res) => {
  const exportId = req.params.exportId
  const sub = req.body.sub
  if (typeof sub !== "string") {
    return res.status(400).json({
      success: false,
      message: "Sub not given correctly",
    })
  }
  try {
    let exportDoc = await Export.findById(exportId).populate("_collection", "owner").exec()
    if (!exportDoc || exportDoc._collection.owner !== sub) {
      return res.status(403).json({
        success: false,
        message: "You don't have permission",
        error: "Forbidden"
      })
    }
    return res.status(200).json({
      success: true,
      message: "Export retrieved successfully",
      data: { export: exportDoc.depopulate("_collection") }
    })
  }
  catch (error) {
    console.log(error)
    return res.status(500).json({
      succeed: false,
      message: "Internal server error"
    })
  }
}


module.exports = {
  getExportsByCollection,
  createExport,
  getNextExport,
  updateExport,
  getExport,
}