const User = require('../models/user')

const createUser = async (req, res) => {
  const body = req.body
  const sub = req.user.sub

  const existingUser = await User.findById(sub).exec()
  if (existingUser) {
    return res.status(400).json({
      success: false,
      message: 'User already existing'
    })
  }

  const user = new User({ _id: sub, ...body })

  if (!user) {
    return res.status(400).json({ success: false, error: err })
  }
  console.log(user)
  try {
    await user.save()
    return res.status(201).json({
      success: true,
      message: 'User created successfully',
      data: { id: user._id },
    })
  } catch (error) {
    console.log(error)
    return res.status(500).json({
      success: false,
      error,
      message: 'User not created',
    })
  }
}

const deleteUser = async (req, res) => {
  await User.findOneAndDelete({ _id: req.params.id }, (err, user) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }
    if (!user) {
      return res.status(404).json({ success: false, error: `User not found` })
    }
    return res.status(200).json({ success: true, message: `User deleted` })
  })
}

const getUserById = async (req, res) => {
  await User.findOne({ _id: req.params.id }, (err, user) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }

    if (!user) {
      return res
        .status(404)
        .json({ success: false, error: `User not found` })
    }
    return res.status(200).json({ success: true, data: user })
  }).catch(err => console.log(err))
}

const getUsers = async (req, res) => {
  User.find({}, (err, users) => {
    if (err) {
      return res.status(400).json({ success: false, error: err })
    }
    if (!users.length) {
      return res
        .status(404)
        .json({ success: false, error: `User not found` })
    }
    return res.status(200).json({ success: true, data: users })
  })
}

module.exports = {
  createUser,
  deleteUser,
  getUsers,
  getUserById,
}