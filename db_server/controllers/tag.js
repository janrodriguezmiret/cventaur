const Tag = require("../models/tag")

const getAllTags = async (req, res) => {
  try {
    let tags = await Tag.find().sort({ className: 1 }).exec()
    return res.status(200).json({
      success: true,
      message: "Tags retrieved successfully",
      data: { tags },
    })
  }
  catch (error) {
    console.log(error)
    return res.status(500).json({
      error: error.message,
      message: 'Cannot get tags',
      success: false
    })
  }
}

const createTag = async (req, res) => {
  const { tag } = req.body
  try {
    if (typeof tag !== "object") {
      return res.status(400).json({
        success: false,
        message: "Tag not given",
      })
    }
    const tagDoc = await Tag.create(tag)
    return res.status(201).json({
      success: true,
      message: 'Tag created successfully',
      data: { tag: tagDoc }
    })
  }
  catch (error) {
    if (error.name === "ValidationError") {
      return res.status(400).json({
        success: false,
        message: 'Data not valid',
        error: error.message
      })
    }
    console.log(error)
    return res.status(500).json({
      error: error.message,
      message: 'Cannot create tag',
      success: false
    })
  }
}

module.exports = {
  getAllTags,
  createTag
}