const Collection = require("../models/collection")
const Tag = require("../models/tag")
const mongoose = require("mongoose")
const db = require("../db/index")


const createCollection = async (req, res) => {
  let sub, collection;
  ({ sub, collection } = req.body)
  if (typeof collection !== "object" || typeof sub !== "string") {
    return res.status(400).json({
      success: false,
      error: 'Collection or sub not given',
    })
  }
  let session = null;
  try {
    const tags = collection.tags
    if (!tags || tags.length === 0) {
      return res.status(400).json({
        message: 'Some fields not given',
        success: false,
        error: "Tags not given (at least one)",
      })
    }
    await Promise.all([Collection.createCollection(), Tag.createCollection()])
    session = await db.startSession()
    session.startTransaction()

    const collectionDoc = new Collection({ owner: sub, ...collection })
    await collectionDoc.save({ session: session })
    await session.commitTransaction()
    const populatedCollection = await Collection.findById(collectionDoc._id).populate("tags").exec()
    return res.status(201).json({
      success: true,
      message: 'Collection created successfully',
      data: { collection: populatedCollection },
    })
  }
  catch (error) {
    console.log(error)
    if (session != null) {
      await session.abortTransaction()
    }
    if (error instanceof TypeError || error.name === "ValidationError") {
      return res.status(400).json({
        message: 'Some fields not given',
        success: false,
        error: error.message,
      })
    }
    return res.status(500).json({
      message: 'Collection not created',
      success: false,
      error: error.message,
    })
  }
}

const getCollectionsByOwner = async (req, res) => {
  const user_id = req.params.owner_id
  const sub = req.query.sub
  if (typeof sub !== "string") {
    return res.status(400).json({
      success: false,
      message: "Sub not given"
    })
  }
  if (sub !== user_id) {
    return res.status(403).json({
      success: false,
      message: "Forbidden"
    })
  }
  try {
    let collections = await Collection.find({ owner: user_id })
      .populate("tags")
      .exec()
    return res.status(200).json({
      success: true,
      message: "Collections successfully retrieved",
      data: { collections },
    })
  }
  catch (error) {
    console.log(error)
    return res.status(500).json({
      error,
      message: 'Cannot get collections',
      success: false,
    })
  }
}

const deleteCollection = async (req, res) => {
  const sub = req.body.sub
  const collection_id = req.params.collection_id
  if (typeof sub !== "string") {
    return res.status(400).json({
      success: false,
      message: "Sub not given"
    })
  }
  try {
    let collection = await Collection.findOne({ _id: collection_id }).exec()
    if (!collection) {
      return res.status(400).json({ success: false, message: "Collection does not exist" })
    }
    if (collection.owner !== sub) {
      return res.status(403).json({ success: false, message: "Forbidden" })
    }
    await collection.remove()
    return res.status(200).json({ success: true, message: "Collection deleted" })
  }
  catch (error) {
    console.log(error)
    if (!mongoose.Types.ObjectId.isValid(collection_id)) {
      return res.status(400).json({ success: false, message: "Collection does not exist" })
    }
    return res.status(500).json({
      message: 'Collection not deleted',
      success: false,
    })
  }
}

const getBaseCollections = async (req, res) => {
  try {
    let baseCollections = await Collection.find({ isBase: true }).populate("tags").exec()
    return res.status(200).json({
      success: true,
      data: { baseCollections },
    })
  }
  catch (error) {
    console.log(error)
    return res.status(500).json({
      error,
      message: 'Cannot get base collections',
    })
  }
}

module.exports = {
  createCollection,
  getCollectionsByOwner,
  deleteCollection,
  getBaseCollections,
}