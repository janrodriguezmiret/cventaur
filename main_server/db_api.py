import requests
import json

base_url = 'http://localhost:8080/api'


def create_user(sub, collection_id):
    data = {'sub': sub}
    return requests.post(base_url+f'/collections/{collection_id}/jobs', json=data)


def create_collection(sub, collection):
    data = {'sub': sub, 'collection': collection}
    return requests.post(base_url+'/collections', json=data)


def get_collections_by_owner(sub, owner_id):
    data = {'sub': sub}
    return requests.get(base_url+f'/collections/owner/{owner_id}', params=data)


def delete_collection(sub, collection_id):
    data = {'sub': sub}
    return requests.delete(base_url+f'/collections/{collection_id}', json=data)


def create_job(sub, img_props, collection_id):
    data = {'sub': sub, 'imgProps': img_props}
    return requests.post(base_url+f'/collections/{collection_id}/jobs', json=data)


def get_jobs(sub, collection_id, confirmed):
    data = {'sub': sub, 'confirmed': confirmed}
    return requests.get(base_url+f'/collections/{collection_id}/jobs', params=data)


def update_job(job):
    data = {'job': job}
    job_id = job['_id']
    return requests.put(base_url+f'/jobs/{job_id}', json=data)


def make_prediction(validation_id, objects):
    data = {'objects': objects}
    return requests.post(base_url+f'/validations/{validation_id}/prediction', json=data)


def get_next_jobs(num_jobs):
    data = {'numJobs': num_jobs}
    return requests.get(base_url+f'/jobs/next', params=data)


def get_base_collections():
    return requests.get(base_url+f'/collections/base')


def get_all_tags():
    return requests.get(base_url+f'/tags')


def create_tag(className):
    data = {'tag': className}
    return requests.post(base_url+f'/tags', json=data)


def get_validations(sub, job_id):
    data = {'sub': sub}
    return requests.get(base_url+f'/validations/job/{job_id}', params=data)


def update_job_validations(sub, job_id, validations):
    data = {'sub': sub, 'validations': validations}
    return requests.patch(base_url+f'/validations/job/{job_id}', json=data)


def update_job(job_id, job_changes, sub=None):
    if sub is None:
        data = {'jobChanges': job_changes}
    else:
        data = {'sub': sub, 'jobChanges': job_changes}
    return requests.patch(base_url+f'/jobs/{job_id}', json=data)


def create_export(sub, collection_id, job_ids):
    data = {'sub': sub, 'collectionId': collection_id, 'jobIds': job_ids}
    return requests.post(base_url+f'/exports', json=data)


def get_next_exports():
    return requests.get(base_url+f'/exports/next')


def update_export(export_id, export_changes):
    data = {'exportChanges': export_changes}
    return requests.patch(base_url+f'/exports/{export_id}', json=data)


def get_exports_by_collection(sub, collection_id):
    data = {'sub': sub}
    return requests.get(base_url+f'/exports/collection/{collection_id}', json=data)


def get_export(sub, export_id):
    data = {'sub': sub}
    return requests.get(base_url+f'/exports/{export_id}', json=data)


def create_model(sub, collection_id):
    data = {'sub': sub}
    return requests.post(base_url+f'/models/collection/{collection_id}', json=data)
