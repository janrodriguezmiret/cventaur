import os
import time
from shutil import copytree, ignore_patterns, copy2, make_archive, rmtree

import requests
from imageai.Detection.Custom import CustomObjectDetection

import db_api
from config import COLLECTIONS_DIR, NUM_JOBS, TIME_SLEEP
from json_to_xml import generateXML


def execute_job(job):
  """Executes a waiting job, making predictions over its images"""
  print(job)
  # Update state to processing in database
  job_changes = {'state': "processing"}
  response = db_api.update_job(job['_id'],job_changes)

  # Load the model
  collection_dir = os.path.join(COLLECTIONS_DIR,job['_collection'])
  detector = CustomObjectDetection()
  detector.setModelTypeAsYOLOv3()
  detector.setModelPath(os.path.join(collection_dir,"models","model.h5"))
  detector.setJsonPath(os.path.join(collection_dir,"json","detection_config.json"))
  try:
    detector.loadModel()

    # Execute prediction on all images of the job
    images_dir = os.path.join(collection_dir,'jobs',job['_id'],'images')
    for image in os.listdir(images_dir):
      input_image = os.path.join(images_dir,image)
      _,objects = detector.detectObjectsFromImage(input_image=input_image, output_type="array",minimum_percentage_probability=30)

      # Rename fields
      renamed_objects = [{
        'name': object_detected["name"],
        'prob': object_detected["percentage_probability"],
        'bbox': object_detected["box_points"],
      } for object_detected in objects]
      # Save prediction to database
      validation_id = image.split('.')[0]
      response = db_api.make_prediction(validation_id,renamed_objects)
      if response.status_code != 200:
        print(response)
        return
  # If the model or the detection_config does not exist (because not uploaded yet), then no predictions are made
  # The validations are already created when the job is created, and they are what is shown on the web, not predictions.
  except FileNotFoundError as error:
    pass
  finally:
    # Once finished, update state to "processed" in database
    job_changes = {'state': "processed", 'numPredicted': job["numTotal"]}
    response = db_api.update_job(job['_id'],job_changes)


def execute_export(export):
  """Executes a waiting Export, mounting the correspondent zip file"""
  print(export)
  export_id = export['_id']
  # Update Export state to 'processing' in database
  export_changes = {'state': "processing"}
  response = db_api.update_export(export_id,export_changes)
  # Get collection_id from one of its Jobs (all Jobs are from the same collection)
  jobs = export['jobs']
  collection_id = export['jobs'][0]['_collection']
  # Create Export's directory for the zip
  exports_dir = os.path.join(COLLECTIONS_DIR,collection_id,'exports')
  export_dir = os.path.join(exports_dir,export_id)
  export_images_dir = os.path.join(export_dir,'images')
  export_annotations_dir = os.path.join(export_dir,'annotations')
  os.makedirs(export_images_dir)
  os.makedirs(export_annotations_dir)

  # Append the resulting files (XML and Images) of each Job
  for job in jobs:
    job_dir = os.path.join(COLLECTIONS_DIR,collection_id,'jobs',job['_id'])
    annotations_dir = os.path.join(job_dir,'annotations')
    images_dir = os.path.join(job_dir,'images')
    # If Job has validations, it means that its XMLs either don't exist or they aren't up-to-date,
    # so we need to generate and save them on the Job directory to use them for later exports
    if 'validations' in job:
      for validation in job['validations']:
        dest_xml_path = os.path.join(annotations_dir,validation["_id"]) + ".xml"
        generateXML(validation,dest_xml_path)

    # Now, all XMLs exist and are up-to-date. Copy files
    for image in os.listdir(images_dir):
      source_image_path = os.path.join(images_dir,image)
      dest_image_path = os.path.join(export_images_dir,image)
      copy2(source_image_path,dest_image_path)
    for annotation in os.listdir(annotations_dir):
      source_xml_path = os.path.join(annotations_dir,annotation)
      dest_xml_path = os.path.join(export_annotations_dir,annotation)
      copy2(source_xml_path,dest_xml_path)

  # Zip the directory and delete files
  make_archive(export_dir,'zip',export_dir)
  rmtree(export_dir)

  # Update Export on database (it will update the Job's 'lastExport' field)
  export_changes = {'state': "processed" }
  response = db_api.update_export(export_id,export_changes)


def work():
  while True:
    try:
      # Find waiting Jobs and execute them
      response = db_api.get_next_jobs(NUM_JOBS)
      jobs = response.json()['data']['jobs']
      print(f'Number of jobs: {len(jobs)}')
      for job in jobs:
        execute_job(job)
        print(f'Job {job["_id"]} finished')

      # Find waiting exports and build them
      response = db_api.get_next_exports()
      exports = response.json()['data']['exports']
      print(f'Number of exports: {len(exports)}')
      for export in exports:
        execute_export(export)
        print(f'Export {export["_id"]} finished')

      # Sleep if nothing left to do
      if not jobs and not exports:
        time.sleep(TIME_SLEEP)

    except Exception as error:
      print(error)
      time.sleep(TIME_SLEEP)

if __name__ == "__main__":
  work()
