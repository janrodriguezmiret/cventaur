COLLECTIONS_DIR = "collections" # The root directory where all collections are stored
PORT = 9090
NUM_JOBS = 1 # How many Jobs are requested to execute to database
TIME_SLEEP = 60 # Seconds to sleep if nothing to execute