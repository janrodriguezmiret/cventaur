import xml.etree.ElementTree as ET

def indent(elem, level=0):
  """Adapted version from code extracted from: http://effbot.org/zone/element-lib.htm#prettyprint"""

  i = "\n" + level*"\t"
  if len(elem):
    if not elem.text or not elem.text.strip():
      elem.text = i + "\t"
    if not elem.tail or not elem.tail.strip():
      elem.tail = i
    for elem in elem:
      indent(elem, level+1)
    if not elem.tail or not elem.tail.strip():
      elem.tail = i
  else:
    if level and (not elem.tail or not elem.tail.strip()):
      elem.tail = i

def generateXML(validation,dst_file):
  """Generate the corresponding XML Pascal VOC file in 'dst_file' with the given 'json' data"""

  print(f"Generating XML {dst_file}")

  annotation = ET.Element("annotation")

  folder = ET.SubElement(annotation,"folder")
  folder.text = "train"

  filename = ET.SubElement(annotation,"filename")
  filename.text = validation["image"]["filename"]

  path = ET.SubElement(annotation,"path")
  path.text = validation["image"]["filename"]

  source = ET.SubElement(annotation,"source")
  database = ET.SubElement(source,"database")
  database.text = "Unknown"

  size = ET.SubElement(annotation,"size")
  width = ET.SubElement(size,"width")
  width.text = str(validation["image"]["width"])
  height = ET.SubElement(size,"height")
  height.text = str(validation["image"]["height"])
  depth = ET.SubElement(size,"depth")
  depth.text = str(3)

  segmented = ET.SubElement(annotation,"segmented")
  segmented.text = str(0)

  for object_detected in validation["objects"]:
    object_elem = ET.SubElement(annotation,"object")

    name = ET.SubElement(object_elem,"name")
    name.text = object_detected["tag"]["className"]

    pose = ET.SubElement(object_elem,"pose")
    pose.text = "Unspecified"

    truncated = ET.SubElement(object_elem,"truncated")
    truncated.text = str(0)

    difficult = ET.SubElement(object_elem,"difficult")
    difficult.text = str(0)

    bndbox = ET.SubElement(object_elem,"bndbox")
    xmin = ET.SubElement(bndbox,"xmin")
    xmin.text = str(object_detected["bbox"][0])
    ymin = ET.SubElement(bndbox,"ymin")
    ymin.text = str(object_detected["bbox"][1])
    xmax = ET.SubElement(bndbox,"xmax")
    xmax.text = str(object_detected["bbox"][2])
    ymax = ET.SubElement(bndbox,"ymax")
    ymax.text = str(object_detected["bbox"][3])

  # Make it prettier by adding new lines and white spaces
  indent(annotation)

  tree = ET.ElementTree(annotation)
  tree.write(dst_file)

  