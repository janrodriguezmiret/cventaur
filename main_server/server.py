import json
import os
import shutil
import warnings
from functools import wraps

from flask import Flask, _request_ctx_stack, jsonify, request, Response, send_file
from flask_cors import cross_origin, CORS
from jose import jwt
from six.moves.urllib.request import urlopen
from PIL import Image

from config import COLLECTIONS_DIR, PORT
import db_api

warnings.simplefilter(action='ignore', category=FutureWarning)

AUTH0_DOMAIN = 'dev-2c08sgwe.eu.auth0.com'
API_AUDIENCE = 'https://dev-2c08sgwe.eu.auth0.com/api/v2/'
ALGORITHMS = ["RS256"]

app = Flask(__name__)
CORS(app)

# Error handler


class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


@app.errorhandler(AuthError)
def handle_auth_error(ex):
    response = jsonify(ex.error)
    response.status_code = ex.status_code
    return response

# Format error response and append status code


def get_token_auth_header():
    """Obtains the Access Token from the Authorization Header
    """
    auth = request.headers.get("Authorization", None)
    if not auth:
        raise AuthError({"code": "authorization_header_missing",
                         "description": "Authorization header is expected"}, 401)

    parts = auth.split()

    if parts[0].lower() != "bearer":
        raise AuthError({"code": "invalid_header",
                         "description": "Authorization header must start with Bearer"
                         }, 401)
    elif len(parts) == 1:
        raise AuthError({"code": "invalid_header",
                         "description": "Token not found"}, 401)
    elif len(parts) > 2:
        raise AuthError({"code": "invalid_header",
                         "description":
                         "Authorization header must be Bearer token"}, 401)

    token = parts[1]
    return token


def requires_auth(f):
    """Determines if the Access Token is valid
    """
    @wraps(f)
    def decorated(*args, **kwargs):
        token = get_token_auth_header()
        jsonurl = urlopen("https://"+AUTH0_DOMAIN+"/.well-known/jwks.json")
        jwks = json.loads(jsonurl.read())
        unverified_header = jwt.get_unverified_header(token)
        rsa_key = {}
        for key in jwks["keys"]:
            if key["kid"] == unverified_header["kid"]:
                rsa_key = {
                    "kty": key["kty"],
                    "kid": key["kid"],
                    "use": key["use"],
                    "n": key["n"],
                    "e": key["e"]
                }
        if rsa_key:
            try:
                payload = jwt.decode(
                    token,
                    rsa_key,
                    algorithms=ALGORITHMS,
                    audience=API_AUDIENCE,
                    issuer="https://"+AUTH0_DOMAIN+"/"
                )
            except jwt.ExpiredSignatureError:
                raise AuthError({"code": "token_expired",
                                 "description": "token is expired"}, 401)
            except jwt.JWTClaimsError:
                raise AuthError({"code": "invalid_claims",
                                 "description":
                                 "incorrect claims,"
                                 "please check the audience and issuer"}, 401)
            except Exception:
                raise AuthError({"code": "invalid_header",
                                 "description":
                                 "Unable to parse authentication"
                                 " token."}, 401)

            _request_ctx_stack.top.current_user = payload
            return f(*args, **kwargs)
        raise AuthError({"code": "invalid_header",
                         "description": "Unable to find appropriate key"}, 401)
    return decorated


@app.route('/api/collections', methods=['POST'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def create_collection():
    sub = _request_ctx_stack.top.current_user['sub']
    try:
        collection = request.json['collection']
        response = db_api.create_collection(sub, collection)
        if response.status_code != 201:
            return Response(response=response.content, status=response.status_code, content_type='application/json')
        collection_id = response.json()['data']['collection']['_id']
        # Create directories for the collection
        try:
            os.makedirs(os.path.join(COLLECTIONS_DIR, collection_id, 'models'))
            os.makedirs(os.path.join(COLLECTIONS_DIR, collection_id, 'json'))
            os.makedirs(os.path.join(COLLECTIONS_DIR, collection_id, 'jobs'))
            os.makedirs(os.path.join(COLLECTIONS_DIR,
                                     collection_id, 'train', 'images'))
            os.makedirs(os.path.join(COLLECTIONS_DIR,
                                     collection_id, 'train', 'annotations'))
            os.makedirs(os.path.join(COLLECTIONS_DIR,
                                     collection_id, 'validation', 'images'))
            os.makedirs(os.path.join(COLLECTIONS_DIR,
                                     collection_id, 'validation', 'annotations'))
            return {
                'message': 'Collection resources and models created',
                'succeed': True,
                'data': {
                    'collection': response.json()['data']['collection']
                }
            }, 201
        except FileExistsError as error:
            print(error)
            return {
                'message': 'Collection resources already exist',
                'succeed': False
            }, 400
    except KeyError as error:
        print(error)
        return {
            'message': 'No collection given',
            'succeed': False
        }, 400
    except Exception as error:
        print(error)
        return {
            'succeed': False,
            'error': 'Internal server error'
        }, 500


@app.route('/api/collections/<collection_id>', methods=['DELETE'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def delete_collection(collection_id):
    sub = _request_ctx_stack.top.current_user['sub']
    try:
        response = db_api.delete_collection(sub, collection_id)
        if response.status_code != 200:
            return Response(response=response.content, status=response.status_code, content_type='application/json')
        directory = os.path.join(COLLECTIONS_DIR, collection_id)
        if (not os.path.isdir(directory)):
            print('Collection {} does not exist'.format(collection_id))
            return {
                'message': 'Collection does not exist',
                'succeed': False
            }, 400
        shutil.rmtree(directory)
        print('Collection deleted')
        return {
            'message': 'Collection deleted',
            'succeed': True
        }, 200
    except Exception as e:
        print(e)
        return {
            'message': 'Internal server error',
            'succeed': False
        }, 500


@app.route('/api/collections/owner/<owner_id>', methods=['GET'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def get_collections_by_owner(owner_id):
    sub = _request_ctx_stack.top.current_user['sub']
    response = db_api.get_collections_by_owner(sub, owner_id)
    return Response(response=response.content, status=response.status_code, content_type='application/json')


@app.route('/api/collections/base', methods=['GET'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def get_base_collections():
    response = db_api.get_base_collections()
    return Response(response=response.content, status=response.status_code, content_type='application/json')


@app.route('/api/tags', methods=['GET'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def get_all_tags():
    response = db_api.get_all_tags()
    return Response(response=response.content, status=response.status_code, content_type='application/json')


@app.route('/api/tags', methods=['POST'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def create_tag():
    tag = request.json['tag']
    response = db_api.create_tag(tag)
    return Response(response=response.content, status=response.status_code, content_type='application/json')


@app.route('/api/collections/<collection_id>/jobs', methods=['POST'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def create_job(collection_id):
    sub = _request_ctx_stack.top.current_user['sub']
    if not request.content_type or not request.content_type.startswith('multipart/form-data'):
        return {
            'success': False,
            'message': 'Resources must be in a multipart/form-data'
        }, 400
    try:
        images = request.files.getlist('images')
        img_props = []
        if (not images or not images[0].filename):
            print('no images given')
            return {
                'success': False,
                'message': 'No images given'
            }, 400
        for image in images:
            if (not (image.mimetype == 'image/jpeg' or image.mimetype == 'image/png')):
                print('incorrect image type: only jpg (jpeg) or png')
                return {
                    'success': False,
                    'message': 'Incorrect image type: only jpg (jpeg) or png',
                    'file': image.filename
                }, 400
            img = Image.open(image)
            img_prop = {
                'extension': "png",  # image.filename.split('.')[-1],
                'width': img.width,
                'height': img.height
            }
            img_props.append(img_prop)
        response = db_api.create_job(sub, img_props, collection_id)
        if response.status_code != 201:
            return Response(response=response.content, status=response.status_code, content_type='application/json')
        response_data = response.json()['data']
        job = response_data['job']
        images_ids = response_data['imagesIds']
        # Create directory and subdirectories for job and save images
        job_dir = os.path.join(
            COLLECTIONS_DIR, collection_id, 'jobs', job['_id'])
        images_dir = os.path.join(job_dir,'images')
        annotations_dir = os.path.join(job_dir,'annotations')
        os.makedirs(images_dir)
        os.makedirs(annotations_dir)
        for index in range(len(images)):
            image = images[index]
            filename = images_ids[index] + ".png"
            filepath = os.path.join(images_dir, filename)
            img = Image.open(image)
            img.save(filepath, format="png")
        return {
            'message': 'Job queued successfully',
            'succeed': True,
            'data': {
                'job': job
            }
        }, 201
    except Exception as e:
        print(e)
        return {
            'message': 'Internal server error',
            'succeed': False
        }, 500


@app.route('/api/collections/<collection_id>/jobs', methods=['GET'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def get_jobs(collection_id):
    sub = _request_ctx_stack.top.current_user['sub']
    try:
        confirmed = request.args.get('confirmed')
        response = db_api.get_jobs(sub, collection_id,confirmed)
    except:
        return {
            'success': False,
            'message': 'Internal server error'
        }, 500
    return Response(response=response.content, status=response.status_code, content_type='application/json')


@app.route('/api/validations/job/<job_id>', methods=['GET'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def get_validations(job_id):
    sub = _request_ctx_stack.top.current_user['sub']
    response = db_api.get_validations(sub, job_id)
    return Response(response=response.content, status=response.status_code, content_type='application/json')


@app.route('/api/collections/<collection_id>/jobs/<job_id>/images/<image_id>', methods=['GET'])
def get_job_image(collection_id, job_id, image_id):
    filename = os.path.join(
        COLLECTIONS_DIR, collection_id, 'jobs', job_id, 'images', image_id)
    return send_file(filename_or_fp=filename)


@app.route('/api/validations/job/<job_id>', methods=['PATCH'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def update_job_validations(job_id):
    sub = _request_ctx_stack.top.current_user['sub']
    try:
        validations = request.json['validations']
        response = db_api.update_job_validations(sub, job_id, validations)
    except:
        return {
            'success': False,
            'message': 'No validations given'
        }, 400
    return Response(response=response.content, status=response.status_code, content_type='application/json')


@app.route('/api/jobs/<job_id>', methods=['PATCH'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def update_job(job_id):
    sub = _request_ctx_stack.top.current_user['sub']
    try:
        job_changes = request.json['jobChanges']
        response = db_api.update_job(job_id, job_changes, sub=sub)
        print(response)
        return Response(response=response.content, status=response.status_code, content_type='application/json')
    except:
        return {
            'success': False,
            'message': 'No validations given'
        }, 400

@app.route('/api/exports', methods=['POST'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def create_export():
    sub = _request_ctx_stack.top.current_user['sub']
    try:
        job_ids = request.json['jobIds']
        collection_id = request.json['collectionId']
        response = db_api.create_export(sub, collection_id, job_ids)
        return Response(response=response.content, status=response.status_code, content_type='application/json')
    except KeyError:
        return {
            'success': False,
            'message': 'No jobIds given'
        }, 400
    except Exception as e:
        print(e)
        return {
            'message': 'Internal server error',
            'succeed': False
        }, 500

@app.route('/api/exports/collection/<collection_id>', methods=['GET'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def get_exports_by_collection(collection_id):
    sub = _request_ctx_stack.top.current_user['sub']
    try:
        response = db_api.get_exports_by_collection(sub, collection_id)
        return Response(response=response.content, status=response.status_code, content_type='application/json')
    except Exception as e:
        print(e)
        return {
            'message': 'Internal server error',
            'succeed': False
        }, 500

@app.route('/api/exports/collection/<collection_id>/download/<export_filename>', methods=['GET'])
def get_export(export_filename,collection_id):
    try:
        export_path = os.path.join(COLLECTIONS_DIR,collection_id,'exports',export_filename)
        return send_file(filename_or_fp=export_path,mimetype="application/zip",attachment_filename=export_filename)
    except Exception as e:
        print(e)
        return {
            'message': 'Internal server error',
            'succeed': False
        }, 500

@app.route('/api/models/collection/<collection_id>', methods=['POST'])
@cross_origin(headers=["Content-Type", "Authorization"])
@requires_auth
def create_model(collection_id):
    """
    Overwrite the current model with the new model given, along with its detection_configuration.json.
    """

    sub = _request_ctx_stack.top.current_user['sub']
    try:
        model_file = request.files.getlist('model')[0]
        config_file = request.files.getlist('config')[0]
        response = db_api.create_model(sub, collection_id)
        if response.status_code != 201:
            return Response(response=response.content, status=response.status_code, content_type='application/json')
        response_data = response.json()['data']
        collection = response_data['collection']
        # Overwrite the current model
        model_path = os.path.join(COLLECTIONS_DIR,collection_id,'models','model.h5')
        model_file.save(model_path)
        config_path = os.path.join(COLLECTIONS_DIR,collection_id,'json','detection_config.json')
        config_file.save(config_path)
        return {
            'message': 'Model created successfully',
            'succeed': True,
            'data': {
                'collection': collection
            }
        }, 201
    except IndexError as error:
        print(error)
        return {
            'message': 'No model or config given',
            'succeed': False
        }, 400
    except Exception as e:
        print(e)
        return {
            'message': 'Internal server error',
            'succeed': False
        }, 500

@app.route('/api/templates/cventaur.ipynb', methods=['GET'])
def get_upload_model_template():
    filename = os.path.join(
        'templates', 'cventaur.ipynb')
    return send_file(filename_or_fp=filename)

if __name__ == '__main__':
    app.run(port=PORT, debug=True)
